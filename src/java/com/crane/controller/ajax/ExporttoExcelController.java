/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.controller.ajax;

import com.crane.portal.bean.ClientHeaderDetailsBean;
import com.crane.portal.bean.DocumentBean;
import com.crane.portal.service.ClientListService;
import com.crane.portal.util.Utilities;
import com.google.gson.Gson;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 *
 * @author apavithra
 */
@Controller
public class ExporttoExcelController {

    @Autowired
    @Qualifier("clientManagementProperties")
    private Properties properties;

    @Autowired
    ClientListService clientListService;

    @RequestMapping("/*exportToExcel*")
    public void exportToExcel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String type = (String) request.getSession().getAttribute("type");
        Map searchMap = (HashMap) request.getSession().getAttribute("searchMap");

        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment;filename=ClientList.xlsx");
        Map paramMap = new HashMap();
        paramMap.put("exportExcelFlag", "Y");
        paramMap.put("clientType", type);
        paramMap.put("searchMap", searchMap);
        paramMap.put("orderByColumn", Integer.parseInt((String) request.getSession().getAttribute("orderByColumn")));
        paramMap.put("orderDir", request.getSession().getAttribute("orderDir"));
        paramMap.put("regionOfUser", request.getSession().getAttribute("regionOfUser"));
        String[] columns = {"CLIENT NAME", "CLIENT CODE", "REQUESTED BY", "SALESREP", "STATION", "SUBMIT DATE", "STATUS"};
        Map resultMap = clientListService.getClientList(paramMap);
        List<ClientHeaderDetailsBean> clientDetailsList = (List<ClientHeaderDetailsBean>) resultMap.get("clientList");

        XSSFWorkbook workbook = new XSSFWorkbook();

        //Get first sheet from the workbook
        final XSSFSheet sheet = workbook.createSheet("Client List");
        sheet.setDefaultColumnWidth(30);
        XSSFCellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontHeight((short) (11 * 20));
        font.setFontName("Calibri");

        XSSFColor myColor = new XSSFColor(new java.awt.Color(11, 60, 93));

// get the palette index of that color 
        style.setFillForegroundColor(myColor);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        XSSFCellStyle style2 = workbook.createCellStyle();
        Font font2 = workbook.createFont();
        font2.setFontHeight((short) (11 * 20));

        font2.setFontName("Calibri");

// get the palette index of that color 
        XSSFColor myColor2 = new XSSFColor(new java.awt.Color(233, 243, 253));
        style2.setFillForegroundColor(myColor2);
        style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        style2.setFont(font2);

        XSSFCellStyle style3 = workbook.createCellStyle();

        style3.setFillForegroundColor(HSSFColor.WHITE.index);
        style3.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        style3.setFont(font2);

        XSSFRow header = sheet.createRow(0);
        int colCount = 0;

        colCount = 0;
        for (String column : columns) {

            header.createCell(colCount).setCellValue(column);
            header.getCell(colCount).setCellStyle(style);

            colCount++;

        }
        header.setHeightInPoints((2 * sheet.getDefaultRowHeightInPoints()));
        int rowCount = 1;
        if (clientDetailsList != null && !clientDetailsList.isEmpty()) {
            for (ClientHeaderDetailsBean clientBean : clientDetailsList) {
                XSSFRow aRow = sheet.createRow(rowCount++);
                aRow.createCell(0).setCellValue(clientBean.getClientName());
                aRow.createCell(1).setCellValue(clientBean.getClientCode());
                aRow.createCell(2).setCellValue(clientBean.getRequestedBy());
                aRow.createCell(3).setCellValue(clientBean.getSalesRep());
                aRow.createCell(4).setCellValue(clientBean.getStation());
                aRow.createCell(5).setCellValue(clientBean.getSubmitDate());
                aRow.createCell(6).setCellValue(clientBean.getStatus());

                aRow.setHeightInPoints(((float) 1.5 * sheet.getDefaultRowHeightInPoints()));

            }
        }
        try {

            workbook.write(response.getOutputStream());

            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }

    }

    @RequestMapping("/*uploadFiles*")
    public void uploadFiles(MultipartHttpServletRequest request, HttpServletResponse response, Model model) throws IOException {

        System.out.println("inside" + request.getParameter("fileType"));
            System.out.println("clientId===" + request.getParameter("clientId"));
        String fileType = request.getParameter("fileType");
        String clientId = request.getParameter("clientId");
        Iterator<String> itr = request.getFileNames();

        DocumentBean fileUploadBean = new DocumentBean();
        Map paramMap = new HashMap();
        paramMap.put("clientId", clientId);
        OutputStream outStream = response.getOutputStream();

        while (itr.hasNext()) {
            MultipartFile mpf = request.getFile(itr.next());
            System.out.println(mpf.getOriginalFilename() + " uploaded!");

            fileUploadBean.setDocumentName(mpf.getOriginalFilename());
            fileUploadBean.setActualFileName(mpf.getOriginalFilename());
            fileUploadBean.setDocumentTypeId(fileType);
            fileUploadBean.setDocumentType(Utilities.DOCUMENT_TYPES_MAP.get(fileType));

            String extension = "";
            String fileName = mpf.getOriginalFilename();

            int i = fileName.lastIndexOf('.');
            System.out.println(fileName.substring(0, i));
            if (i > 0) {
                extension = fileName.substring(i + 1);
            }
            Calendar lCDateTime = Calendar.getInstance();
            long destinationFile = lCDateTime.getTimeInMillis();
            String originalFileName = destinationFile + "." + extension;
            fileUploadBean.setActualFileName(originalFileName);
            // fileUploadBean.setUploadedFile(mpf);

            InputStream is = mpf.getInputStream();
            String strFolder_Loc = (String) properties.get("CLIENTDOCS_LOC");
            FileOutputStream fis = new FileOutputStream(strFolder_Loc.trim() + originalFileName);
            int c;
            while ((c = is.read()) != -1) {
                fis.write(c);
            }
            // fis.flush();         

            fis.close();
        }
        paramMap.put("clientId", clientId);
        paramMap.put("fileUploadBean", fileUploadBean);

        List<DocumentBean> fileUploadBeans = clientListService.updateAndRetrieveUploadedDocuments(paramMap);
        request.getSession().setAttribute("fileUploadBeans", fileUploadBeans);
        //clientDetailsBean.setUploadedFiles(fileUploadBeans);
        Gson gson = new Gson();
        outStream.write(gson.toJson(fileUploadBeans).getBytes());
        outStream.flush();
        outStream.close();

    }

    @RequestMapping("/*downloadFile*")
    public void downloadFile(HttpServletRequest request, HttpServletResponse response, Model model) {

        System.out.println("inside download flag!!");
        //PrintWriter out = response.getWriter();
        try {

            try {
                // documentURL = documentURL + "/" + documentName;
                String fileName = (String) request.getParameter("fileName");

                FileInputStream fileToDownload = new FileInputStream(properties.getProperty("CLIENTDOCS_LOC") + fileName);

                ServletOutputStream out = response.getOutputStream();
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment;filename=" + fileName + "");
                response.setContentLength(fileToDownload.available());
                int c;
                while ((c = fileToDownload.read()) != -1) {
                    out.write(c);
                }
                out.flush();
                out.close();
                fileToDownload.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("-----------Exception---------> " + e);
            e.printStackTrace();
        }

    }
     @RequestMapping("/*deleteFiles")
    public void deleteFiles(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String fileName = request.getParameter("fileName");
        final String fileTypeId = request.getParameter("fileTypeId");
        Map paramMap = new HashMap();
        String clientId = request.getParameter("clientId");
        paramMap.put("fileTypeId",fileTypeId);
        paramMap.put("clientId",clientId);
        System.out.println("inside delete files!!!!" + fileName);
     //   String strFolder_Loc = (String) properties.get("CLIENTDOCS_LOC");
        
     
//        List<FileUploadBean> fileUploadBeans = clientDetailsBean.getUploadedFiles()
//                                                .stream()
//                                                .filter(fileBean -> !fileTypeId.equals(fileBean.getFileTypeId()))
//                                                 .collect(Collectors.toList());
//       
       
            
           /*  File file = new File(strFolder_Loc + "//" + fileName);
           if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }*/
        
    


      clientListService.deleteUploadedDocuments(paramMap);
    }
    
    
    

}
