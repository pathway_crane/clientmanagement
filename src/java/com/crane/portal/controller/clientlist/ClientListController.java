/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.controller.clientlist;

import com.crane.portal.bean.ClientHeaderDetailsBean;
import com.crane.portal.bean.ClientInformationBean;
import com.crane.portal.bean.ClientProfileOperationsBean;
import com.crane.portal.bean.ContactBean;
import com.crane.portal.service.ClientListService;
import com.crane.portal.util.Utilities;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.mail.MessagingException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

/**
 *
 * @author apavithra
 */
@Controller
@RequestMapping(value = "VIEW")
public class ClientListController {

    @Autowired
    ClientListService clientListService;

    @Autowired
    Utilities utilities;

    @RenderMapping
    public String displayMainPage(RenderRequest request, RenderResponse response, Model model) throws Exception {
        String viewName = (String) request.getPortletSession().getAttribute("viewName");
        if (viewName != null) {
            request.getPortletSession().removeAttribute(viewName);
        }
        if ("ClientProfile".equals(viewName)) {

            ClientInformationBean clientInformationBean = (ClientInformationBean) request.getPortletSession().getAttribute("clientInformationBean");
            ClientHeaderDetailsBean clientHeaderDetailsBean = clientInformationBean.getClientHeaderBean();

            System.out.println("client name==>" + clientHeaderDetailsBean.getClientName());
            Map paramMap = new HashMap();
            paramMap.put("region", clientHeaderDetailsBean.getRegion());
            List<ContactBean> escalationList = clientListService.getEscalationList(paramMap);
            ContactBean contactBean = new ContactBean();
            contactBean.setContactName(clientHeaderDetailsBean.getRequestedBy());
            contactBean.setEmail(clientHeaderDetailsBean.getInitialRequestorMailId());
            escalationList.add(0, contactBean);
            if (!Utilities.isNullValue(clientHeaderDetailsBean.getLastRequestorMailId())) {
                contactBean = new ContactBean();
                contactBean.setContactName(clientHeaderDetailsBean.getLastRequestorMailId());
                contactBean.setEmail(clientHeaderDetailsBean.getEscalateUserName());
                escalationList.add(1, contactBean);
            }
            String superUserFlag = (String) request.getPortletSession().getAttribute("superUserFlag", PortletSession.APPLICATION_SCOPE);
            System.out.println("superUserFlag===>" + superUserFlag + ",status===>" + clientHeaderDetailsBean.getStatus());
            model.addAttribute("escalationList", escalationList);
            model.addAttribute("approversTypeList", Utilities.approverTypeList);
            model.addAttribute("docTypeList", Utilities.DOCUMENT_TYPES_LIST);
            model.addAttribute("clientInformationBean", clientInformationBean);
            model.addAttribute("superUserFlag", superUserFlag);

            model.addAttribute("userName", request.getRemoteUser());

            return "ClientProfile";
        } else {

            System.out.println("inside displayMainPage!!!!");
            String currentTab = (String) request.getPortletSession().getAttribute("type", PortletSession.APPLICATION_SCOPE);
            if (Utilities.isNullValue(currentTab)) {
                currentTab = "ALL";
            }
            model.addAttribute("currentTab", currentTab);
            if (Utilities.DOCUMENT_TYPES_MAP.isEmpty()) {
                utilities.populateDropdowns();
            }
            request.getPortletSession().removeAttribute("type", PortletSession.APPLICATION_SCOPE);
            request.getPortletSession().removeAttribute("searchMap", PortletSession.APPLICATION_SCOPE);
            request.getPortletSession().removeAttribute("clientInformationBean");
            return "ClientList";
        }

    }

    @ActionMapping(params = "submitType=clientProfile")
    public void fetchClientProfileDetails(ActionRequest request, ActionResponse actionResponse, Model model) {
        System.out.println("inside client profile form");
        String clientId = request.getParameter("clientId");
        Map paramMap = new HashMap();
        paramMap.put("clientId", clientId);
        ClientInformationBean clientInformationBean = clientListService.getClientProfileBean(paramMap);
        request.getPortletSession().setAttribute("viewName", "ClientProfile");
        request.getPortletSession().setAttribute("clientInformationBean", clientInformationBean);

    }

    @ActionMapping(params = "submitType=sendToRequestor")
    public void sendToRequestor(ActionRequest request, ActionResponse actionResponse, Model model) throws MessagingException {
        System.out.println("inside client profile form");
        ClientInformationBean clientInformationBean = (ClientInformationBean) request.getPortletSession().getAttribute("clientInformationBean");
        ClientHeaderDetailsBean clientHeaderBean = clientInformationBean.getClientHeaderBean();
        String clientCode = request.getParameter("clientCode");
        Map paramMap = new HashMap();
        paramMap.put("clientId", clientHeaderBean.getClientId());
        paramMap.put("clientCode", clientCode);

        paramMap.put("clientInformationBean", clientInformationBean);

        clientListService.sendToRequestor(paramMap);
        request.getPortletSession().setAttribute("viewName", "ClientList");

    }

    @ActionMapping(params = "submitType=activateClientId")
    public void activateClientId(ActionRequest request, ActionResponse actionResponse, Model model) throws MessagingException {
        System.out.println("inside activateClientId of client profile form"+request.getParameter("lastReqMailId"));
         System.out.println("inside activateMailContent of client profile form"+request.getParameter("activateMailContent"));
        ClientInformationBean clientInformationBean = (ClientInformationBean) request.getPortletSession().getAttribute("clientInformationBean");
        ClientHeaderDetailsBean clientHeaderBean = clientInformationBean.getClientHeaderBean();
        String clientCode = request.getParameter("clientCode");
        Map paramMap = new HashMap();
        paramMap.put("clientId", clientHeaderBean.getClientId());
        paramMap.put("clientCode", clientCode);
        paramMap.put("content", request.getParameter("activateMailContent"));
        paramMap.put("emailIds", request.getParameter("lastReqMailId"));
        paramMap.put("loggedInUserName", request.getRemoteUser());

        paramMap.put("clientInformationBean", clientInformationBean);

        clientListService.activateClientId(paramMap);
        request.getPortletSession().setAttribute("viewName", "ClientList");

    }

    @ActionMapping(params = "submitType=backToClientList")
    public void viewClientListPage(ActionRequest request, ActionResponse actionResponse, Model model) throws MessagingException {

        request.getPortletSession().setAttribute("viewName", "ClientList");

    }

    @ResourceMapping("clientList")
    public void getClientList(ResourceRequest request, ResourceResponse response) throws IOException {
        Gson gson = new Gson();
        Map paramMap = new HashMap();
        OutputStream outStream = response.getPortletOutputStream();
        String type = request.getParameter("clientType");
        System.out.println("type===>" + type);
        String region = (String) request.getPortletSession().getAttribute("regionOfUser", PortletSession.APPLICATION_SCOPE);
        String superUserFlag = (String) request.getPortletSession().getAttribute("superUserFlag", PortletSession.APPLICATION_SCOPE);
        //   Map<String, String[]> requestParams = request.getParameterMap();

        String totalColumns = request.getParameter("totalColumns");
        Map<String, String> searchMap = null;
        if (Utilities.checkNumeric(totalColumns)) {
            int colsCount = Integer.parseInt(totalColumns);

            for (int i = 0; i < colsCount; i++) {
                String searchValue = request.getParameter("columns[" + i + "][search][value]");
                if (!Utilities.isNullValue(searchValue)) {
                    if (searchMap == null) {
                        searchMap = new HashMap<>();
                    }
                    searchMap.put(request.getParameter("columns[" + i + "][data]"), searchValue);
                    System.out.println(request.getParameter("columns[" + i + "][data]") + "=" + searchValue);
                }

            }
            if (searchMap != null) {
                paramMap.put("searchMap", searchMap);
            }
        }
        String orderByColumn = request.getParameter("order[0][column]");
        String orderDir = request.getParameter("order[0][dir]");
        if (Utilities.isNullValue(orderByColumn)) {
            orderByColumn = "0";
        }
        if (Utilities.isNullValue(orderDir)) {
            orderDir = "ASC";
        }
        paramMap.put("orderByColumn", Integer.parseInt(orderByColumn) + 1);
        paramMap.put("orderDir", orderDir);
        //     Set<Map.Entry<String, String[]>> entrySet = requestParams.entrySet();
        /* for(Map.Entry<String, String[]> entry:entrySet){
            System.out.print(entry.getKey()+":");
            System.out.println(entry.getValue().toString());
            
        }*/
        if ("Y".equals(superUserFlag)) {
            region = "ALL";
        }
        paramMap.put("regionOfUser", region);
        System.out.println("startLength==" + request.getParameter("start"));
        System.out.println("length==>" + request.getParameter("length"));
        int startIndex = Integer.parseInt(request.getParameter("start"));
        int length = Integer.parseInt(request.getParameter("length"));
        paramMap.put("startIndex", startIndex + 1);
        paramMap.put("endIndex", startIndex + length);
        String prevType = (String) request.getPortletSession().getAttribute("type", PortletSession.APPLICATION_SCOPE);
        if (prevType != null && prevType.equals(type)) {
            paramMap.put("totalCount", request.getPortletSession().getAttribute("totalCount"));
            Map prevSearchMap = (HashMap) request.getPortletSession().getAttribute("searchMap", PortletSession.APPLICATION_SCOPE);
            if (startIndex >= length || (searchMap != null && searchMap.equals(prevSearchMap))) {
                paramMap.put("filteredCount", request.getPortletSession().getAttribute("filteredCount"));
            }
        } else {
            paramMap.put("totalCount", 0);
        }
        paramMap.put("clientType", type);

        Map resultMap = clientListService.getClientList(paramMap);
        List<ClientHeaderDetailsBean> clientDetailsList = (List<ClientHeaderDetailsBean>) resultMap.get("clientList");
        int totalCount = (Integer) resultMap.get("totalCount");
        String jsonStr = gson.toJson(clientDetailsList);
        StringBuilder strBuilder = new StringBuilder();
        int filteredCount = resultMap.containsKey("filteredCount") ? (Integer) resultMap.get("filteredCount") : totalCount;
        strBuilder.append("{").append(" \"recordsTotal\":").append(totalCount)
                .append(",\"recordsFiltered\":").append(filteredCount)
                .append(", \"data\":").append(jsonStr).append("}");
        outStream.write(strBuilder.toString().getBytes());
        outStream.flush();
        outStream.close();
        request.getPortletSession().setAttribute("type", type, PortletSession.APPLICATION_SCOPE);
        request.getPortletSession().setAttribute("orderByColumn", orderByColumn + 1, PortletSession.APPLICATION_SCOPE);
        request.getPortletSession().setAttribute("orderDir", orderDir, PortletSession.APPLICATION_SCOPE);
        request.getPortletSession().setAttribute("totalCount", totalCount);
        request.getPortletSession().setAttribute("filteredCount", filteredCount);
        request.getPortletSession().setAttribute("searchMap", searchMap, PortletSession.APPLICATION_SCOPE);

    }

    @ResourceMapping("addNewComment")
    public void addNewComment(ResourceRequest request, ResourceResponse response) {
        Map paramMap = new HashMap();
        ClientInformationBean clientInformationBean = (ClientInformationBean) request.getPortletSession().getAttribute("clientInformationBean");
        ClientHeaderDetailsBean clientHeaderBean = clientInformationBean.getClientHeaderBean();
        String comments = request.getParameter("comments");
        paramMap.put("comments", comments);
        paramMap.put("userName", request.getRemoteUser());
        paramMap.put("clientId", clientHeaderBean.getClientId());
        clientListService.insertNewComment(paramMap);

    }

    @ResourceMapping("escalateClientId")
    public void escalateClientId(ResourceRequest request, ResourceResponse response) throws MessagingException {
        System.out.println("Escalation By category called");
        Map paramMap = new HashMap();
        ClientInformationBean clientInformationBean = (ClientInformationBean) request.getPortletSession().getAttribute("clientInformationBean");
        ClientHeaderDetailsBean clientHeaderBean = clientInformationBean.getClientHeaderBean();
        String approverType = request.getParameter("approverType");
        String escalationType = request.getParameter("escalationType");
        String escalatedUserName = request.getParameter("escalatedUserName");
        String escalateUserMail = request.getParameter("escalateUserMail");
        String region = clientHeaderBean.getRegion();
        String salesStation = clientHeaderBean.getStation();
        String comments = request.getParameter("comments");
        String additionalEmails = request.getParameter("additionalEmailIds");
        if (!Utilities.isNullValue(additionalEmails)) {
            paramMap.put("additionalMailIds", Stream.of(additionalEmails.split(",")).collect(Collectors.toList()));

        }
        System.out.println("comments==>" + comments);

        paramMap.put("region", region);
        paramMap.put("salesStation", salesStation);
        paramMap.put("comments", comments);
        paramMap.put("clientId", clientHeaderBean.getClientId());
        paramMap.put("userName", request.getRemoteUser());

        paramMap.put("clientInformationBean", clientInformationBean);
        if ("escalateByName".equals(escalationType)) {
            paramMap.put("toName", escalatedUserName);
            paramMap.put("escalateUserMail", escalateUserMail);
            clientListService.escalateByName(paramMap);
        } else {
            paramMap.put("approverType", approverType);
            clientListService.escalateByCategory(paramMap);
        }

    }
     @ResourceMapping("updateClientName")
     public void updateClientName(ResourceRequest request, ResourceResponse response){
           ClientInformationBean clientInformationBean = (ClientInformationBean) request.getPortletSession().getAttribute("clientInformationBean");
        ClientHeaderDetailsBean clientHeaderBean = clientInformationBean.getClientHeaderBean();
         String clientId = clientHeaderBean.getClientId();
         String clientCode = request.getParameter("clientCode");
         String clientName = request.getParameter("clientName");
         Map paramMap = new HashMap();
         paramMap.put("clientId",clientId);
         paramMap.put("clientCode",clientCode);
         paramMap.put("clientName",clientName);
         clientListService.updateClientName(paramMap);
         
     }
    @ExceptionHandler(Exception.class)
	public ModelAndView handleAllException(Exception ex) {

		ModelAndView model = new ModelAndView("Error");
		return model;

	}

}
