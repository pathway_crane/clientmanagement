/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.service;

import com.crane.portal.bean.ClientInformationBean;
import com.crane.portal.bean.ContactBean;
import com.crane.portal.bean.DocumentBean;
import java.util.List;
import java.util.Map;
import javax.mail.MessagingException;

/**
 *
 * @author apavithra
 */
public interface ClientListService {

    public Map getClientList(Map paramMap);

    public ClientInformationBean getClientProfileBean(Map paramMap);

    public List<ContactBean> getEscalationList(Map paramMap);

    public void escalateByCategory(Map paramMap) throws MessagingException;

    public void escalateByName(Map paramMap) throws MessagingException;

    public void insertNewComment(Map paramMap);

    public void sendToRequestor(Map paramMap) throws MessagingException;

    public void deleteUploadedDocuments(Map paramMap);

    public List<DocumentBean> updateAndRetrieveUploadedDocuments(Map paramMap);

    public void activateClientId(Map paramMap) throws MessagingException;
    
     public void updateClientName(Map paramMap);
}
