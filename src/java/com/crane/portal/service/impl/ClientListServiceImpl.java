/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.service.impl;

import com.crane.portal.bean.ClientHeaderDetailsBean;
import com.crane.portal.bean.ClientInformationBean;
import com.crane.portal.bean.ContactBean;
import com.crane.portal.bean.CreditInformationBean;
import com.crane.portal.bean.DocumentBean;
import com.crane.portal.dao.ClientListDAO;
import com.crane.portal.service.ClientListService;
import com.crane.portal.util.SendMail;
import com.crane.portal.util.Utilities;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author apavithra
 */
@Service
public class ClientListServiceImpl implements ClientListService {
    
    @Autowired
    ClientListDAO clientListDAO;
    
    SendMail sendMail;
    
    public ClientListServiceImpl() {
        sendMail = new SendMail();
    }
    
    public Map getClientList(Map paramMap) {
        return clientListDAO.getClientList(paramMap);
    }
    
    public ClientInformationBean getClientProfileBean(Map paramMap) {
        return clientListDAO.getClientProfileBean(paramMap);
    }
    
    public List<ContactBean> getEscalationList(Map paramMap) {
        return clientListDAO.getEscalationList(paramMap);
    }
    
    private void addEscalateCommentAndSendMail(Map paramMap) throws MessagingException {
        List escalationList = (List) paramMap.get("escalationList");
        List additionalEmailList = (List) paramMap.get("additionalMailIds");
        String comments = (String) paramMap.get("comments");
        String toName = (String) paramMap.get("toName");
        clientListDAO.insertNewComment(paramMap);
        
        ClientInformationBean clientInformationBean = (ClientInformationBean) paramMap.get("clientInformationBean");
        ClientHeaderDetailsBean clientHeaderDetailsBean = clientInformationBean.getClientHeaderBean();
        String tcsubject = "Customer Setup Escalation:  " + clientHeaderDetailsBean.getClientName();
        
        StringBuilder mailBuilder = new StringBuilder();
        mailBuilder.append("Dear ").append(toName).append(",<br/><br/>The following Customer Setup escalation is pending for your review and approval. <br/><br/>").append("Customer: ").append(clientHeaderDetailsBean.getClientName()).append(" <br/>").
                append("Salesperson: ").append(clientHeaderDetailsBean.getSalesRep()).append("<br/>")
                .append("Sales department: ").append(clientHeaderDetailsBean.getStation()).append("SLS<br/>")
                .append("Requested Credit Limit 1: ").append(clientInformationBean.getCreditInformationBean().getBillingCurrency1()).append(clientInformationBean.getCreditInformationBean().getCreditLimit1Req()).append("<br/>")
                // .append(clientInformationBean.getCreditInformationBean().getCreditLimit2Req()).append("<br/>")
                .append("Requested Credit Terms: ").append(clientInformationBean.getCreditInformationBean().getCreditTerm()).append("<br/><br/>")
                .append("Special Instructions:").append(clientInformationBean.getRatingBillingBean().getSpecialInstructions()).append("<br/><br/>")
                .append("Escalation Details:<br/>")
                .append(comments).append("<br/><br/>")
                .append("Please check the details by clicking on the ");
        String url9 = "https://cview.craneww.com/portal/classic/Client_Management/Client_List?portal:isSecure=true&navigationalstate=JBPNS_rO0ABXclAAZhY3Rpb24AAAABAA5jdXN0b21lckRlbmllZAAHX19FT0ZfXw**&interactionstate=JBPNS_rO0ABXc_AAZhY3Rpb24AAAABAChDdXN0b21lck1hbmFnZW1lbnRJbnByb2Nlc3NDdXN0b21lcl92aWV3AAdfX0VPRl9f&portal:componentId=gtnc6c3f7b2-4fc7-4180-85b9-b4717f95fdfd&portal:type=action";
        //  System.out.println("4) From All Customer \n" + "strcreditLimitRequested: " + strcreditLimitRequested + "creditLmtSec: " + creditLmtSec);
        if (additionalEmailList != null && additionalEmailList.size() > 0) {
            escalationList.addAll(additionalEmailList);
        }
        sendMail.sendmailcommon(escalationList, tcsubject, mailBuilder.toString(), url9);
        
    }
    
    public void escalateByCategory(Map paramMap) throws MessagingException {
        String approverType = (String) paramMap.get("approverType");
        
        String toName = approverType.replace("_", " ");
        paramMap.put("toName", toName);
        
        List<String> escalationList = null;
        if (approverType.equals(Utilities.category.ANALYST)) {
            escalationList = new ArrayList<>();
            escalationList.add(Utilities.CLIENT_SETUP_EMAIL);
        } else {
            escalationList = clientListDAO.getApproverMailIds(paramMap);
        }
        paramMap.put("escalationList", escalationList);
        
        addEscalateCommentAndSendMail(paramMap);
    }
    
    public void escalateByName(Map paramMap) throws MessagingException {
        System.out.println("escalateByName in service");
        String escalateUserEmail = (String) paramMap.get("escalateUserMail");
        List<String> escalationList = new ArrayList<>();
        escalationList.add(escalateUserEmail);
        
        paramMap.put("escalationList", escalationList);
        
        addEscalateCommentAndSendMail(paramMap);
    }
    
    public void insertNewComment(Map paramMap) {
        clientListDAO.insertNewComment(paramMap);
    }
    
    public void sendToRequestor(Map paramMap) throws MessagingException {
        ClientInformationBean clientInformationBean = (ClientInformationBean) paramMap.get("clientInformationBean");
        ClientHeaderDetailsBean clientHeaderDetailsBean = clientInformationBean.getClientHeaderBean();
        CreditInformationBean creditInformationBean = clientInformationBean.getCreditInformationBean();
        clientListDAO.setCompletedStatus(paramMap);
        String requestorMailId = clientHeaderDetailsBean.getInitialRequestorMailId();
        String requestor = clientHeaderDetailsBean.getRequestedBy();
        String tcsubject = "Customer Setup:  " + clientHeaderDetailsBean.getClientName();
        StringBuilder mailContentStr = new StringBuilder();
        mailContentStr.append("Dear ").append(requestor).append(",<br/>Your Customer Setup request for ").append(clientHeaderDetailsBean.getClientName()).append(" / client code ").append(clientHeaderDetailsBean.getClientCode()).append(" with credit term ").append(clientInformationBean.getCreditInformationBean().getCreditTerm()).append(" and credit limit1 ").append(creditInformationBean.getCreditLimit1Req());
        if (!Utilities.isNullValue(creditInformationBean.getCreditLimit2Req())) {
            mailContentStr.append(" and credit limit2 ").append(creditInformationBean.getCreditLimit2Req());
        }
        mailContentStr.append(" has been activated.  <br/><br/>")
                .append("Notes:<br/>")
                .append("You can request to automate the Customer Tariff in Kewill System for regular shipments in strengthening execution efficiency.<br/> ")
                .append("If you have a tariff with this customer, please send the rates and requirements to customertariff@craneww.com for review and action.<br/><br/>")
                .append("You can request for an EDI automation via Concierge Portal > Applications > EDI.<br/> ")
                .append("https://craneww.atlassian.net/servicedesk/customer/portal/7/group/26/create/91 <br/>")
                .append("Please check the details by clicking on the ");
        
        String url = "https://cview.craneww.com/portal/classic/Client_Management/Client_List?portal:isSecure=true&navigationalstate=JBPNS_rO0ABXc_AAZhY3Rpb24AAAABAChDdXN0b21lck1hbmFnZW1lbnRJbnByb2Nlc3NDdXN0b21lcl92aWV3AAdfX0VPRl9f&interactionstate=JBPNS_rO0ABXdAAAZhY3Rpb24AAAABAClDdXN0b21lck1hbmFnZW1lbnRBbGxDdXN0b21lclBvcnRsZXRfdmlldwAHX19FT0ZfXw**&portal:componentId=gtnc6c3f7b2-4fc7-4180-85b9-b4717f95fdfd&portal:type=action";
        List<String> recipientList = Stream.of(new String[]{requestorMailId, Utilities.CLIENT_SETUP_EMAIL}).collect(Collectors.toList());
        sendMail.sendmailcommon(recipientList, tcsubject, mailContentStr.toString(), url);
        
    }
    
    public List<DocumentBean> updateAndRetrieveUploadedDocuments(Map paramMap) {
        return clientListDAO.updateAndRetrieveUploadedDocuments(paramMap);
    }
    
    public void deleteUploadedDocuments(Map paramMap) {
        clientListDAO.deleteUploadedDocuments(paramMap);
    }

    public void activateClientId(Map paramMap) throws MessagingException {
        String emailIds = (String) paramMap.get("emailIds");
        String clientCode = (String) paramMap.get("clientCode");
        String content = (String) paramMap.get("content");
       clientListDAO.activateClientId(paramMap);
        String[] recipients = emailIds.split(",");
        String tcsubject = "Customer Activation:  " + clientCode;
        content = content + "<br/>";
        String url11 = "https://cview.craneww.com/portal/classic/Client_Management/Client_List?portal:isSecure=true&navigationalstate=JBPNS_rO0ABXc_AAZhY3Rpb24AAAABAChDdXN0b21lck1hbmFnZW1lbnRJbnByb2Nlc3NDdXN0b21lcl92aWV3AAdfX0VPRl9f&interactionstate=JBPNS_rO0ABXdAAAZhY3Rpb24AAAABAClDdXN0b21lck1hbmFnZW1lbnRBbGxDdXN0b21lclBvcnRsZXRfdmlldwAHX19FT0ZfXw**&portal:componentId=gtnc6c3f7b2-4fc7-4180-85b9-b4717f95fdfd&portal:type=action";
        sendMail.sendmailcommon(Stream.of(recipients).collect(Collectors.toList()), tcsubject, content, url11);
    }
     public void updateClientName(Map paramMap){
         clientListDAO.updateClientName(paramMap);
     }
}
