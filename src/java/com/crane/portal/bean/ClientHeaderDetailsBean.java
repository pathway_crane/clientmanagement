/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.bean;

/**
 *
 * @author apavithra
 */
public class ClientHeaderDetailsBean {
    private String clientCode;
    private String clientName;
    private String clientId;
    private String requestedBy;
    private String salesRep;
    private String station;
    private String submitDate;
    private String status;
    private String clientType;
    private String industry;
    private String subsector;
    private String taxId;
    private String initialRequestorMailId;
    private String lastRequestorMailId;
    private String escalateUserName;
    private String region;
    private String approverType;
    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getApproverType() {
        return approverType;
    }

    public void setApproverType(String approverType) {
        this.approverType = approverType;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLastRequestorMailId() {
        return lastRequestorMailId;
    }

    public void setLastRequestorMailId(String lastRequestorMailId) {
        this.lastRequestorMailId = lastRequestorMailId;
    }

    public String getEscalateUserName() {
        return escalateUserName;
    }

    public void setEscalateUserName(String escalateUserName) {
        this.escalateUserName = escalateUserName;
    }

    public String getInitialRequestorMailId() {
        return initialRequestorMailId;
    }

    public void setInitialRequestorMailId(String initialRequestorMailId) {
        this.initialRequestorMailId = initialRequestorMailId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getSalesRep() {
        return salesRep;
    }

    public void setSalesRep(String salesRep) {
        this.salesRep = salesRep;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getSubsector() {
        return subsector;
    }

    public void setSubsector(String subsector) {
        this.subsector = subsector;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }
    
}
