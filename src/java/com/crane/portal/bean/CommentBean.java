/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.bean;

/**
 *
 * @author apavithra
 */
public class CommentBean {

    private String commentType;
    private String comment;
    private String commentFromUserId;
    private String commentToUserId;
    private String requestorMailId;
    private String commentDate;
    private String denyComment;
    private String commentId;
    private String commentTime;

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getDenyComment() {
        return denyComment;
    }

    public void setDenyComment(String denyComment) {
        this.denyComment = denyComment;
    }

    public String getCommentType() {
        return commentType;
    }

    public void setCommentType(String commentType) {
        this.commentType = commentType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentFromUserId() {
        return commentFromUserId;
    }

    public void setCommentFromUserId(String commentFromUserId) {
        this.commentFromUserId = commentFromUserId;
    }

    public String getCommentToUserId() {
        return commentToUserId;
    }

    public void setCommentToUserId(String commentToUserId) {
        this.commentToUserId = commentToUserId;
    }

    public String getRequestorMailId() {
        return requestorMailId;
    }

    public void setRequestorMailId(String requestorMailId) {
        this.requestorMailId = requestorMailId;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

}
