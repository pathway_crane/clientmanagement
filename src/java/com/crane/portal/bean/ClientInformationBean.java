/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.bean;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apavithra
 */
public class ClientInformationBean {
    private ClientHeaderDetailsBean clientHeaderBean;
    private AddressBean physicalAddress;
    private AddressBean billingAddress;
    private ContactBean executiveContact;
    private ContactBean operationsContact;
    private ContactBean apContact;
    private CreditInformationBean creditInformationBean;
    private List<DocumentBean> documentsList;
    private List<TradeReferenceBean> tradeReferencesList;
    
    public ClientInformationBean(){
        clientHeaderBean=new ClientHeaderDetailsBean();
        physicalAddress = new AddressBean();
        billingAddress = new AddressBean();
        executiveContact = new ContactBean();
        operationsContact = new ContactBean();
        apContact = new ContactBean();
        creditInformationBean = new CreditInformationBean();
        documentsList= new ArrayList<>();
        tradeReferencesList = new ArrayList<>();
        ratingBillingBean = new RatingBillingBean();
    }

    public ContactBean getExecutiveContact() {
        return executiveContact;
    }

    public void setExecutiveContact(ContactBean executiveContact) {
        this.executiveContact = executiveContact;
    }

    public ContactBean getOperationsContact() {
        return operationsContact;
    }

    public void setOperationsContact(ContactBean operationsContact) {
        this.operationsContact = operationsContact;
    }

    public ContactBean getApContact() {
        return apContact;
    }

    public void setApContact(ContactBean apContact) {
        this.apContact = apContact;
    }

    public CreditInformationBean getCreditInformationBean() {
        return creditInformationBean;
    }

    public void setCreditInformationBean(CreditInformationBean creditInformationBean) {
        this.creditInformationBean = creditInformationBean;
    }

    public List<DocumentBean> getDocumentsList() {
        return documentsList;
    }

    public void setDocumentsList(List<DocumentBean> documentsList) {
        this.documentsList = documentsList;
    }

    public List<TradeReferenceBean> getTradeReferencesList() {
        return tradeReferencesList;
    }

    public void setTradeReferencesList(List<TradeReferenceBean> tradeReferencesList) {
        this.tradeReferencesList = tradeReferencesList;
    }

    public RatingBillingBean getRatingBillingBean() {
        return ratingBillingBean;
    }

    public void setRatingBillingBean(RatingBillingBean ratingBillingBean) {
        this.ratingBillingBean = ratingBillingBean;
    }

    public List<CommentBean> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<CommentBean> commentsList) {
        this.commentsList = commentsList;
    }
    private RatingBillingBean ratingBillingBean;
    private List<CommentBean> commentsList;

    public ClientHeaderDetailsBean getClientHeaderBean() {
        return clientHeaderBean;
    }

    public void setClientHeaderBean(ClientHeaderDetailsBean clientHeaderBean) {
        this.clientHeaderBean = clientHeaderBean;
    }

    public AddressBean getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(AddressBean physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public AddressBean getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(AddressBean billingAddress) {
        this.billingAddress = billingAddress;
    }
    
}
