/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.bean;

/**
 *
 * @author apavithra
 */
public class RatingBillingBean {
    private String standardTC;
    private String billingDocs;
    private String invoicingStmt;
    private String cafStmt;
    private String standardRatingStmt;
    private String form;
    private String ediInvoicingStmt;
    private String fscPolicyStmt;
    private String tariffIdStmt;
    private String c2cWarehouseStmt;
    private String specialInstructions;

    public String getStandardTC() {
        return standardTC;
    }

    public void setStandardTC(String standardTC) {
        this.standardTC = standardTC;
    }

    public String getBillingDocs() {
        return billingDocs;
    }

    public void setBillingDocs(String billingDocs) {
        this.billingDocs = billingDocs;
    }

    public String getInvoicingStmt() {
        return invoicingStmt;
    }

    public void setInvoicingStmt(String invoicingStmt) {
        this.invoicingStmt = invoicingStmt;
    }

    public String getCafStmt() {
        return cafStmt;
    }

    public void setCafStmt(String cafStmt) {
        this.cafStmt = cafStmt;
    }

    public String getStandardRatingStmt() {
        return standardRatingStmt;
    }

    public void setStandardRatingStmt(String standardRatingStmt) {
        this.standardRatingStmt = standardRatingStmt;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getEdiInvoicingStmt() {
        return ediInvoicingStmt;
    }

    public void setEdiInvoicingStmt(String ediInvoicingStmt) {
        this.ediInvoicingStmt = ediInvoicingStmt;
    }

    public String getFscPolicyStmt() {
        return fscPolicyStmt;
    }

    public void setFscPolicyStmt(String fscPolicyStmt) {
        this.fscPolicyStmt = fscPolicyStmt;
    }

    public String getTariffIdStmt() {
        return tariffIdStmt;
    }

    public void setTariffIdStmt(String tariffIdStmt) {
        this.tariffIdStmt = tariffIdStmt;
    }

    public String getC2cWarehouseStmt() {
        return c2cWarehouseStmt;
    }

    public void setC2cWarehouseStmt(String c2cWarehouseStmt) {
        this.c2cWarehouseStmt = c2cWarehouseStmt;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }
    
    
    
}
