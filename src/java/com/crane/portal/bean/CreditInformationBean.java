/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.bean;

/**
 *
 * @author apavithra
 */
public class CreditInformationBean {
    private String owner;
    private String yearInBusiness;
    private String businessType;
    private String bankName;
    private String accountNo;
    private String bankPhoneNo;
    private String contact;
    private String creditTerm;
    private String creditLimit1Req;
    private String billingCurrency1;
    private String creditLimit2Req;
    private String billingCurrency2;
    private String creditLimit1Approved;
    private String creditLimit2Approved;
    private String accountType;
    private String accountCategory1;
    private String accountCategory2;
    private String dbNumber1;
    private String dbNumber2;
    private String consLimit1;
    private String consLimit2;
    private String aggrLimit1;
    private String aggrLimit2;
    private String conservativeLimitPaydexAdj1;
    private String conservativeLimitPaydexAdj2;
    private String aggressLimitPaydexAdj1;
    private String aggressLimitPaydexAdj2;

    public String getAggressLimitPaydexAdj1() {
        return aggressLimitPaydexAdj1;
    }

    public void setAggressLimitPaydexAdj1(String aggressLimitPaydexAdj1) {
        this.aggressLimitPaydexAdj1 = aggressLimitPaydexAdj1;
    }

    public String getAggressLimitPaydexAdj2() {
        return aggressLimitPaydexAdj2;
    }

    public void setAggressLimitPaydexAdj2(String aggressLimitPaydexAdj2) {
        this.aggressLimitPaydexAdj2 = aggressLimitPaydexAdj2;
    }

    public String getConsLimit1() {
        return consLimit1;
    }

    public void setConsLimit1(String consLimit1) {
        this.consLimit1 = consLimit1;
    }

    public String getConsLimit2() {
        return consLimit2;
    }

    public void setConsLimit2(String consLimit2) {
        this.consLimit2 = consLimit2;
    }

    public String getAggrLimit1() {
        return aggrLimit1;
    }

    public void setAggrLimit1(String aggrLimit1) {
        this.aggrLimit1 = aggrLimit1;
    }

    public String getAggrLimit2() {
        return aggrLimit2;
    }

    public void setAggrLimit2(String aggrLimit2) {
        this.aggrLimit2 = aggrLimit2;
    }

    public String getConservativeLimitPaydexAdj1() {
        return conservativeLimitPaydexAdj1;
    }

    public void setConservativeLimitPaydexAdj1(String conservativeLimitPaydexAdj1) {
        this.conservativeLimitPaydexAdj1 = conservativeLimitPaydexAdj1;
    }

    public String getConservativeLimitPaydexAdj2() {
        return conservativeLimitPaydexAdj2;
    }

    public void setConservativeLimitPaydexAdj2(String conservativeLimitPaydexAdj2) {
        this.conservativeLimitPaydexAdj2 = conservativeLimitPaydexAdj2;
    }
    

    public String getDbNumber1() {
        return dbNumber1;
    }

    public void setDbNumber1(String dbNumber1) {
        this.dbNumber1 = dbNumber1;
    }

    public String getDbNumber2() {
        return dbNumber2;
    }

    public void setDbNumber2(String dbNumber2) {
        this.dbNumber2 = dbNumber2;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getYearInBusiness() {
        return yearInBusiness;
    }

    public void setYearInBusiness(String yearInBusiness) {
        this.yearInBusiness = yearInBusiness;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBankPhoneNo() {
        return bankPhoneNo;
    }

    public void setBankPhoneNo(String bankPhoneNo) {
        this.bankPhoneNo = bankPhoneNo;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCreditTerm() {
        return creditTerm;
    }

    public void setCreditTerm(String creditTerm) {
        this.creditTerm = creditTerm;
    }

    public String getCreditLimit1Req() {
        return creditLimit1Req;
    }

    public void setCreditLimit1Req(String creditLimit1Req) {
        this.creditLimit1Req = creditLimit1Req;
    }

    public String getBillingCurrency1() {
        return billingCurrency1;
    }

    public void setBillingCurrency1(String billingCurrency1) {
        this.billingCurrency1 = billingCurrency1;
    }

    public String getCreditLimit2Req() {
        return creditLimit2Req;
    }

    public void setCreditLimit2Req(String creditLimit2Req) {
        this.creditLimit2Req = creditLimit2Req;
    }

    public String getBillingCurrency2() {
        return billingCurrency2;
    }

    public void setBillingCurrency2(String billingCurrency2) {
        this.billingCurrency2 = billingCurrency2;
    }

    public String getCreditLimit1Approved() {
        return creditLimit1Approved;
    }

    public void setCreditLimit1Approved(String creditLimit1Approved) {
        this.creditLimit1Approved = creditLimit1Approved;
    }

    public String getCreditLimit2Approved() {
        return creditLimit2Approved;
    }

    public void setCreditLimit2Approved(String creditLimit2Approved) {
        this.creditLimit2Approved = creditLimit2Approved;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountCategory1() {
        return accountCategory1;
    }

    public void setAccountCategory1(String accountCategory1) {
        this.accountCategory1 = accountCategory1;
    }

    public String getAccountCategory2() {
        return accountCategory2;
    }

    public void setAccountCategory2(String accountCategory2) {
        this.accountCategory2 = accountCategory2;
    }
    
    
    
}
