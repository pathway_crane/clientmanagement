/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.bean;

/**
 *
 * @author apavithra
 */
public class ClientProfileOperationsBean {
    private String clientId;
    private String region;
    private String escalateType;
    private String approverType;
    private String escalateUserName;
    private String comments;
    private String clientCode;
    private String additionalEmails;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getEscalateType() {
        return escalateType;
    }

    public void setEscalateType(String escalateType) {
        this.escalateType = escalateType;
    }

    public String getApproverType() {
        return approverType;
    }

    public void setApproverType(String approverType) {
        this.approverType = approverType;
    }

    public String getEscalateUserName() {
        return escalateUserName;
    }

    public void setEscalateUserName(String escalateUserName) {
        this.escalateUserName = escalateUserName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getAdditionalEmails() {
        return additionalEmails;
    }

    public void setAdditionalEmails(String additionalEmails) {
        this.additionalEmails = additionalEmails;
    }
    
    
}
