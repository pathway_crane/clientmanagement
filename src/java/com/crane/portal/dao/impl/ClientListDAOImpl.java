/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.dao.impl;

import com.crane.portal.bean.AddressBean;
import com.crane.portal.bean.ClientHeaderDetailsBean;
import com.crane.portal.bean.ClientInformationBean;
import com.crane.portal.bean.CommentBean;
import com.crane.portal.bean.ContactBean;
import com.crane.portal.bean.CreditInformationBean;
import com.crane.portal.bean.DocumentBean;
import com.crane.portal.bean.RatingBillingBean;
import com.crane.portal.bean.TradeReferenceBean;
import com.crane.portal.dao.ClientListDAO;
import com.crane.portal.util.Utilities;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 *
 * @author apavithra
 */
@Repository
public class ClientListDAOImpl implements ClientListDAO {

    @Autowired
    @Qualifier("jdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("clientManagementProperties")
    private Properties properties;

    @Autowired
    Utilities utilities;

    private String buildSearchConditionBuilder(Map<String, String> searchMap) {
        StringBuilder conditionQuery = new StringBuilder();

        searchMap.entrySet().forEach(entry -> {
            String columnName = properties.getProperty(entry.getKey());
            if(columnName.toLowerCase().contains("date"))
                 conditionQuery.append(" and TRUNC(").append(columnName).append(")>=TO_DATE('").append(entry.getValue()).append("','mm/dd/yyyy')");
            else
                 conditionQuery.append(" and REGEXP_LIKE(").append(columnName).append(",'").append(entry.getValue().replace("'", "''")).append("','i')");
        });

        return conditionQuery.toString();

    }

    public int getTotalCount(Map paramMap) {
        String countQuery = "select count(*) from cs_customer a where archiveflag=0  ";
        String conditionQuery = (String) paramMap.get("conditionQuery");
        if (!Utilities.isNullValue(conditionQuery)) {
            countQuery = countQuery + conditionQuery;

        }

        int totalCount = jdbcTemplate.queryForObject(countQuery, (ResultSet resultset, int i) -> {
            return resultset.getInt(1);
        });
        System.out.println("getting total Count==>" + totalCount);
        return totalCount;
    }

    public List<String> getApproverMailIds(Map paramMap) {
        String approverType = (String) paramMap.get("approverType");
        String region = (String) paramMap.get("approverType");
        String station = (String) paramMap.get("approverType");
        String sqlQuery = "select distinct approver_mail from cs_approvers a , CS_REGION_APPROVER_MAPPING r where a.APPROVER_ID=r.APPROVER_ID ";
        switch (Utilities.category.valueOf(approverType)) {
            case CENTRAL_APPROVER:
                sqlQuery += " and CENTRAL_APPROVER='Y'";
                break;
            case REGIONAL_APPROVER:
                sqlQuery += " and REGIONAL_APPROVER='Y' and region='" + region + "'";
                break;
            case REGIONAL_ACCOUNTS:
                sqlQuery += " and REGIONAL_ACCOUNTS='Y' and region='" + region + "'";
                break;
            case ECS_APPROVER:
                sqlQuery += " and ECS_APPROVER='Y' ";
                break;

        }

        List<String> approversList = jdbcTemplate.query(sqlQuery, (ResultSet resultSet, int i) -> {
            return resultSet.getString(1);
        });
        if ("MEX".equalsIgnoreCase(station) || "GDL".equalsIgnoreCase(station)) {
            approversList.remove("Osney.Silva@craneww.com");
            approversList.add("Kathy.Martinez@craneww.com");
        }
        return approversList;

    }

    public int getPKIDCSComment() {

        String sql = "select max(cs_customerid) from cs_customercomment";
        System.out.println("max of id of comment table is " + jdbcTemplate.queryForInt(sql));

        return jdbcTemplate.queryForInt(sql);

    }

    public void setCompletedStatus(Map paramMap) {
        String clientCode = (String) paramMap.get("clientCode");
        String clientId = (String) paramMap.get("clientId");
        String sqlCustomerUpdate = "UPDATE cs_customer SET status = 'Completed', clientCode = ? "
                + "WHERE id = ? ";
        jdbcTemplate.update(sqlCustomerUpdate, new Object[]{clientCode, clientId});

    }

    public void insertNewComment(Map paramMap) {
        String comment = (String) paramMap.get("comments");
        String userName = (String) paramMap.get("userName");
        String toName = (String) paramMap.get("toName");
        String clientId = (String) paramMap.get("clientId");
        int commentId = getPKIDCSComment() + 1;
        String sql = "Insert into cs_customercomment(cs_customerid,customerId,cs_comment,commentDate,deniedby,toName)"
                + "VALUES (?,?,?,current_date,?,?)";
        jdbcTemplate.update(sql, new Object[]{
            commentId, clientId, comment, userName, toName
        });
    }

    public List<ContactBean> getEscalationList(Map paramMap) {
        String region = (String) paramMap.get("region");
        String sqlQuery = "select distinct approver_name,approver_mail from cs_approvers a , CS_REGION_APPROVER_MAPPING r where a.APPROVER_ID=r.APPROVER_ID and  (REGION=? or ESCALATION_FLAG='ALL')";
        List<ContactBean> escalationList = jdbcTemplate.query(sqlQuery, (ResultSet resultSet, int i) -> {
            ContactBean contactBean = new ContactBean();
            contactBean.setContactName(resultSet.getString(1));
            contactBean.setEmail(resultSet.getString(2));
            return contactBean;

        }, new Object[]{region});
        return escalationList;

    }

    public Map getClientList(Map paramMap) {

        String type = (String) paramMap.get("clientType");
        String exportToExcelFlag = (String) paramMap.get("exportExcelFlag");
        String region = (String) paramMap.get("regionOfUser");
        Integer orderByColumn = (Integer) paramMap.get("orderByColumn");
        String sortDir = (String) paramMap.get("orderDir");

        Map searchMap = (HashMap) paramMap.get("searchMap");
        StringBuilder conditionQuery = new StringBuilder();
        Map resultMap = new HashMap();
        if (!"ALL".equals(type)) {
            conditionQuery.append(" and status='").append(type).append("' ");
        }
        if (!"ALL".equals(region)) {
            if (region.equals("NORTAM") || region.equals("MAGNO")) {
                conditionQuery.append(" and a.salesstation || 'SLS' in (select  deptid from  webportal.cwl_selling_station_lookup_v where REGION in ('NORTAM','Magno'))");
            } else {
                conditionQuery.append(" and a.salesstation || 'SLS' in (select  deptid from  webportal.cwl_selling_station_lookup_v where REGION ='").append(region).append("')");
            }

        } else {
            conditionQuery.append("and a.salesstation || 'SLS' in (select  deptid from  webportal.cwl_selling_station_lookup_v)");
        }

        String sqlQuery = "";
        Object[] queryParams = new Object[]{};
        if ("Y".equals(exportToExcelFlag)) {
            if (searchMap != null && !searchMap.isEmpty()) {
                conditionQuery.append(buildSearchConditionBuilder(searchMap));

            }
            //sqlQuery = "SELECT  CUSTOMERNAME, CLIENTCODE, REQUESTEDBY, SALESREP, SALESSTATION, status,submitDate, TO_CHAR(submitDate, 'MM/DD/YYYY')SUBMIT_DATE,ID,archiveflag FROM cs_customer a WHERE archiveflag=0  " + conditionQuery.toString() + " ORDER BY  " + orderByColumn + "  " + sortDir;
            sqlQuery = "SELECT  CUSTOMERNAME, CLIENTCODE, REQUESTEDBY, SALESREP, SALESSTATION, status,submitDate, TO_CHAR(submitDate, 'MM/DD/YYYY')SUBMIT_DATE,ID,archiveflag FROM cs_customer a WHERE archiveflag=0  " + conditionQuery.toString() ;
        } else {
            int startIndex = (Integer) paramMap.get("startIndex");
            int endIndex = (Integer) paramMap.get("endIndex");
            int totalCount = (Integer) paramMap.get("totalCount");
            paramMap.put("conditionQuery", conditionQuery.toString());
            if (totalCount == 0) {
                totalCount = getTotalCount(paramMap);
            }
            resultMap.put("totalCount", totalCount);
            System.out.println("totalCount in getclient list==>" + totalCount);
            if (searchMap != null && !searchMap.isEmpty()) {
                int filteredCount = 0;
                conditionQuery.append(buildSearchConditionBuilder(searchMap));
                if (paramMap.containsKey("filteredCount")) {
                    filteredCount = (Integer) paramMap.get("filteredCount");
                    System.out.println("Filtred count already fetched. Count is " + filteredCount);
                } else {
                    System.out.println("Invoking filter query---->");

                    paramMap.put("conditionQuery", conditionQuery.toString());
                    filteredCount = getTotalCount(paramMap);

                }
                resultMap.put("filteredCount", filteredCount);
            }
            sqlQuery = "SELECT * FROM (SELECT t.*, ROWNUM AS rn FROM (SELECT CUSTOMERNAME, CLIENTCODE, REQUESTEDBY, SALESREP, SALESSTATION, status,submitDate,  TO_CHAR(submitDate, 'MM/DD/YYYY')SUBMIT_DATE,ID,archiveflag FROM cs_customer a WHERE archiveflag=0 " + conditionQuery.toString() + " ORDER BY  " + orderByColumn + "  " + sortDir + " )t ) WHERE  rn        >=? AND rn        <=?";
            queryParams = new Object[]{startIndex, endIndex};
        }
        System.out.println("sqlQuery==>" + sqlQuery);

        List<ClientHeaderDetailsBean> clientList = jdbcTemplate.query(sqlQuery, (ResultSet resultSet, int i) -> {
            ClientHeaderDetailsBean clientDetailsBean = new ClientHeaderDetailsBean();
            clientDetailsBean.setClientId(Utilities.returnEmptyStringIfNull(resultSet.getString("ID")));
            clientDetailsBean.setClientName(Utilities.returnEmptyStringIfNull(resultSet.getString("CUSTOMERNAME")));
            clientDetailsBean.setClientCode(Utilities.returnEmptyStringIfNull(resultSet.getString("CLIENTCODE")));
            clientDetailsBean.setSalesRep(Utilities.returnEmptyStringIfNull(resultSet.getString("SALESREP")));
            clientDetailsBean.setRequestedBy(Utilities.returnEmptyStringIfNull(resultSet.getString("REQUESTEDBY")));
            clientDetailsBean.setStation(Utilities.returnEmptyStringIfNull(resultSet.getString("SALESSTATION")));
            clientDetailsBean.setSubmitDate(Utilities.returnEmptyStringIfNull(resultSet.getString("SUBMIT_DATE")));
            clientDetailsBean.setStatus(Utilities.returnEmptyStringIfNull(resultSet.getString("STATUS")));

            return clientDetailsBean;
        }, queryParams);

        resultMap.put("clientList", clientList);
        return resultMap;
    }

    public List<CommentBean> getCommentsList(String clientId) {
        String query = "select CUSTOMERID,cs_comment,To_Char(commentDate,'YYYY-MM-DD') as commentDate,To_Char(commentDate,'HH-MI-SS') as commentTime,deny,deniedBy,toName from cs_customercomment b where CUSTOMERID=" + clientId + " order by cs_customerid desc";
        List<CommentBean> commentsList = jdbcTemplate.query(query, (ResultSet resultSet, int i) -> {
            CommentBean commentBean = new CommentBean();
            commentBean.setCommentId(resultSet.getString("customerid"));
            commentBean.setComment(resultSet.getString("cs_comment"));
            commentBean.setCommentDate(resultSet.getString("commentDate"));
            commentBean.setCommentFromUserId(resultSet.getString("deniedBy"));
            commentBean.setDenyComment(resultSet.getString("deny"));
            commentBean.setCommentToUserId(resultSet.getString("toName"));
            commentBean.setCommentTime(resultSet.getString("commentTime"));
            return commentBean;
        });
        return commentsList;
    }

    public ClientInformationBean getClientProfileBean(Map paramMap) {
        String clientId = (String) paramMap.get("clientId");
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("select a.id as CLIENTID,a.customerName,CLIENTCODE,regular_Credit_Customer,requestedBy,a.salesRep,salesstation ,customerIndustry,subsector as subSectorCode, (select region from webportal.cwl_selling_station_lookup_v where deptid=salesstation||'SLS')REGION,")
                .append("clientCode,requestedMailId,TO_CHAR(submitDate, 'YYYY-MM-DD') as submitDate,companyReg ,status,")
                //Physical Address
                .append(" a.physicalAddress1,a.physicalAddress2,a.physicalAddress3,physicalAddress4,pcountryCode,plocationCode,pstatecode,")
                .append("billingAddress1,billingAddress2,billingAddress3,billingAddress4,bcountryCode,blocationCode,bstatecode,")
                //Contact
                .append("executiveContact,executiveMail,executivePhone,executiveMobile,executiveFax,operationContact,operationMail,operationPhone,operationMobile,operationFax,apContact,apMail,apPhone,apMobile,apFax,")
                //Bank Details
                .append("parentCompanyName,yearInBusiness,typeOfBusiness,bankPhone,bankContact,bankName,bankaccount,")
                //Credit Information
                .append("dbNumber,conservativeLimit,aggrLimit,conservativeLimitPaydexAdj,aggrLimitPaydexAdj,")
                .append("dbNumber2,conservativeLimit2,aggrLimit2,conservativeLimitPaydexAdj2,aggrLimitPaydexAdj2,")
                .append("terms,creditLimitRequested,a.creditLimitApproved,(select f.code from cs_currency f where a.billingCurrency1=f.id) as cur1,")
                .append("creditLimitRequestedOther,creditLimitApproved2, submitDate ,(select f.code from cs_currency f where a.billingCurrency2=f.id) as cur2,")
                .append(" accountType,accountCategory,accountCategorySec,")
                //Trade Reference details            
                .append(" companyName1,companyPhone1,companyContact1,companyFax1,companyName2,companyPhone2,")
                .append(" companyContact2,companyFax2,companyFax1,companyName3,companyPhone3,companyContact3,companyFax3,")
                .append("standardTC, standardTariff,tarrifID,docRequiredBilling,form1,form2,form3,form4,statementInvoice,ediInvoice,cafPolicy,fscPolicy,ctocWarehouse,")
                //document Information
                .append(" specialInstruction,SUBSTR(specialInstruction,1,10) as subInstruction ,termsCondition,dnbiReport,salesTariif,other,powerOfAttorney,attachAddittionalSheet,credit_limit_update,unrevoke,")
                //Credit Limit update fields
                .append(" (SELECT REQUESTORMAILID FROM CS_CUSTOMERCOMMENT c WHERE CUSTOMERID=").append(clientId).append("   AND CS_CUSTOMERID = (SELECT MAX(CS_CUSTOMERID) FROM CS_CUSTOMERCOMMENT WHERE  CUSTOMERID   =c.CUSTOMERID and (REQUESTTYPE='UNREVOKE' or REQUESTTYPE='CREDITLIMITUPDATE')))lastRequestorMailId,(SELECT DENIEDBY FROM CS_CUSTOMERCOMMENT c WHERE CUSTOMERID=").append(clientId).append("   AND CS_CUSTOMERID= (SELECT MAX(CS_CUSTOMERID) FROM CS_CUSTOMERCOMMENT WHERE CUSTOMERID =c.CUSTOMERID ) ) ESCALATEUSERNAME ")
                .append(" from cs_customer a,cs_currency c ").append(" where a.billingCurrency1=c.id and a.id=").append(clientId);
        //   String sqlQuery = "select a.id as customerId,a.customerName,requestedBy,a.salesRep,salesstation as code,a.creditLimitApproved,creditLimitApproved2,"
        System.out.println("sql query==>" + sqlQuery.toString());

        List<ClientInformationBean> clientInformationBeansList = jdbcTemplate.query(sqlQuery.toString(), (ResultSet resultSet, int i) -> {
            ClientInformationBean clientInformationBean = new ClientInformationBean();
            ClientHeaderDetailsBean clientHeaderDetailsBean = clientInformationBean.getClientHeaderBean();

            clientHeaderDetailsBean.setClientCode(Utilities.returnEmptyStringIfNull(resultSet.getString("CLIENTCODE")));
            clientHeaderDetailsBean.setClientId(Utilities.returnEmptyStringIfNull(resultSet.getString("CLIENTID")));
            clientHeaderDetailsBean.setClientName(Utilities.returnEmptyStringIfNull(resultSet.getString("customerName")));
            clientHeaderDetailsBean.setClientType("Y".equals(Utilities.returnEmptyStringIfNull(resultSet.getString("regular_Credit_Customer"))) ? "Regular Credit" : "One Time COD Client");
            clientHeaderDetailsBean.setRequestedBy(Utilities.returnEmptyStringIfNull(resultSet.getString("requestedBy")));
            clientHeaderDetailsBean.setSalesRep(Utilities.returnEmptyStringIfNull(resultSet.getString("salesRep")));
            clientHeaderDetailsBean.setStation(Utilities.returnEmptyStringIfNull(resultSet.getString("salesstation")));
            clientHeaderDetailsBean.setIndustry(Utilities.returnEmptyStringIfNull(resultSet.getString("customerIndustry")));
            clientHeaderDetailsBean.setSubsector(Utilities.returnEmptyStringIfNull(resultSet.getString("subSectorCode")));
            clientHeaderDetailsBean.setInitialRequestorMailId(Utilities.returnEmptyStringIfNull(resultSet.getString("requestedMailId")));
            clientHeaderDetailsBean.setSubmitDate(Utilities.returnEmptyStringIfNull(resultSet.getString("submitDate")));
            clientHeaderDetailsBean.setTaxId(Utilities.returnEmptyStringIfNull(resultSet.getString("companyReg")));
            clientHeaderDetailsBean.setStatus(Utilities.returnEmptyStringIfNull(resultSet.getString("status")));
            clientHeaderDetailsBean.setLastRequestorMailId(Utilities.returnEmptyStringIfNull(resultSet.getString("lastRequestorMailId")));
            clientHeaderDetailsBean.setEscalateUserName(Utilities.returnEmptyStringIfNull(resultSet.getString("ESCALATEUSERNAME")));
            clientHeaderDetailsBean.setRegion(Utilities.returnEmptyStringIfNull(resultSet.getString("region")));

            AddressBean physicalAddressBean = clientInformationBean.getPhysicalAddress();
            physicalAddressBean.setAddress1(Utilities.returnEmptyStringIfNull(resultSet.getString("physicalAddress1")));
            physicalAddressBean.setAddress2(Utilities.returnEmptyStringIfNull(resultSet.getString("physicalAddress2")));
            physicalAddressBean.setAddress3(Utilities.returnEmptyStringIfNull(resultSet.getString("physicalAddress3")));
            physicalAddressBean.setAddress4(Utilities.returnEmptyStringIfNull(resultSet.getString("physicalAddress4")));
            physicalAddressBean.setCountry(Utilities.returnEmptyStringIfNull(resultSet.getString("pcountryCode")));
            physicalAddressBean.setLocationCode(Utilities.returnEmptyStringIfNull(resultSet.getString("plocationCode")));
            physicalAddressBean.setState(Utilities.returnEmptyStringIfNull(resultSet.getString("pstatecode")));

            AddressBean billingAddressBean = clientInformationBean.getBillingAddress();
            billingAddressBean.setAddress1(Utilities.returnEmptyStringIfNull(resultSet.getString("billingAddress1")));
            billingAddressBean.setAddress2(Utilities.returnEmptyStringIfNull(resultSet.getString("billingAddress2")));
            billingAddressBean.setAddress3(Utilities.returnEmptyStringIfNull(resultSet.getString("billingAddress3")));
            billingAddressBean.setAddress4(Utilities.returnEmptyStringIfNull(resultSet.getString("billingAddress4")));
            billingAddressBean.setCountry(Utilities.returnEmptyStringIfNull(resultSet.getString("bcountryCode")));
            billingAddressBean.setLocationCode(Utilities.returnEmptyStringIfNull(resultSet.getString("blocationCode")));
            billingAddressBean.setState(Utilities.returnEmptyStringIfNull(resultSet.getString("bstatecode")));

            ContactBean executiveContactBean = clientInformationBean.getExecutiveContact();
            executiveContactBean.setContactName(Utilities.returnEmptyStringIfNull(resultSet.getString("executiveContact")));
            executiveContactBean.setEmail(Utilities.returnEmptyStringIfNull(resultSet.getString("executiveMail")));
            executiveContactBean.setPhone(Utilities.returnEmptyStringIfNull(resultSet.getString("executivePhone")));
            executiveContactBean.setMobile(Utilities.returnEmptyStringIfNull(resultSet.getString("executiveMobile")));
            executiveContactBean.setFax(Utilities.returnEmptyStringIfNull(resultSet.getString("executiveFax")));

            ContactBean operationsContactBean = clientInformationBean.getOperationsContact();
            operationsContactBean.setContactName(Utilities.returnEmptyStringIfNull(resultSet.getString("operationContact")));
            operationsContactBean.setEmail(Utilities.returnEmptyStringIfNull(resultSet.getString("operationMail")));
            operationsContactBean.setPhone(Utilities.returnEmptyStringIfNull(resultSet.getString("operationPhone")));
            operationsContactBean.setMobile(Utilities.returnEmptyStringIfNull(resultSet.getString("operationMobile")));
            operationsContactBean.setFax(Utilities.returnEmptyStringIfNull(resultSet.getString("operationFax")));

            ContactBean apContactBean = clientInformationBean.getApContact();
            apContactBean.setContactName(Utilities.returnEmptyStringIfNull(resultSet.getString("apContact")));
            apContactBean.setEmail(Utilities.returnEmptyStringIfNull(resultSet.getString("apMail")));
            apContactBean.setPhone(Utilities.returnEmptyStringIfNull(resultSet.getString("apPhone")));
            apContactBean.setMobile(Utilities.returnEmptyStringIfNull(resultSet.getString("apMobile")));
            apContactBean.setFax(Utilities.returnEmptyStringIfNull(resultSet.getString("apFax")));
            //Bank Details

            CreditInformationBean creditInformationBean = clientInformationBean.getCreditInformationBean();
            creditInformationBean.setOwner(Utilities.returnEmptyStringIfNull(resultSet.getString("parentCompanyName")));
            creditInformationBean.setYearInBusiness(Utilities.returnEmptyStringIfNull(resultSet.getString("yearInBusiness")));
            creditInformationBean.setBusinessType(Utilities.returnEmptyStringIfNull(resultSet.getString("typeOfBusiness")));
            creditInformationBean.setBankPhoneNo(Utilities.returnEmptyStringIfNull(resultSet.getString("bankPhone")));
            creditInformationBean.setContact(Utilities.returnEmptyStringIfNull(resultSet.getString("bankContact")));
            creditInformationBean.setBankName(Utilities.returnEmptyStringIfNull(resultSet.getString("bankName")));
            creditInformationBean.setAccountNo(Utilities.returnEmptyStringIfNull(resultSet.getString("bankaccount")));
            // //Credit Information

            creditInformationBean.setDbNumber1(Utilities.returnEmptyStringIfNull(resultSet.getString("dbNumber")));
            creditInformationBean.setConsLimit1(Utilities.returnEmptyStringIfNull(resultSet.getString("conservativeLimit")));
            creditInformationBean.setAggrLimit1(Utilities.returnEmptyStringIfNull(resultSet.getString("aggrLimit")));
            creditInformationBean.setConservativeLimitPaydexAdj1(Utilities.returnEmptyStringIfNull(resultSet.getString("conservativeLimitPaydexAdj")));
            creditInformationBean.setAggressLimitPaydexAdj1(Utilities.returnEmptyStringIfNull(resultSet.getString("aggrLimitPaydexAdj")));
            creditInformationBean.setDbNumber2(Utilities.returnEmptyStringIfNull(resultSet.getString("dbNumber2")));
            creditInformationBean.setConsLimit2(Utilities.returnEmptyStringIfNull(resultSet.getString("conservativeLimit2")));
            creditInformationBean.setAggrLimit2(Utilities.returnEmptyStringIfNull(resultSet.getString("aggrLimit2")));
            creditInformationBean.setConservativeLimitPaydexAdj2(Utilities.returnEmptyStringIfNull(resultSet.getString("conservativeLimitPaydexAdj2")));
            creditInformationBean.setAggressLimitPaydexAdj2(Utilities.returnEmptyStringIfNull(resultSet.getString("aggrLimitPaydexAdj2")));

            creditInformationBean.setCreditTerm(Utilities.returnEmptyStringIfNull(resultSet.getString("terms")));
            creditInformationBean.setCreditLimit1Req(Utilities.returnEmptyStringIfNull(resultSet.getString("creditLimitRequested")));
            creditInformationBean.setCreditLimit1Approved(Utilities.returnEmptyStringIfNull(resultSet.getString("creditLimitApproved")));
            creditInformationBean.setBillingCurrency1(Utilities.returnEmptyStringIfNull(resultSet.getString("cur1")));
            creditInformationBean.setCreditLimit2Req(Utilities.returnEmptyStringIfNull(resultSet.getString("creditLimitRequestedOther")));
            creditInformationBean.setCreditLimit2Approved(Utilities.returnEmptyStringIfNull(resultSet.getString("creditLimitApproved2")));

            creditInformationBean.setBillingCurrency2(Utilities.returnEmptyStringIfNull(resultSet.getString("cur2")));
            if ("NONE".equalsIgnoreCase(creditInformationBean.getBillingCurrency2())) {
                creditInformationBean.setBillingCurrency2("");
            }
            creditInformationBean.setAccountType(Utilities.returnEmptyStringIfNull(resultSet.getString("accountType")));
            creditInformationBean.setAccountCategory1(Utilities.returnEmptyStringIfNull(resultSet.getString("accountCategory")));
            creditInformationBean.setAccountCategory2(Utilities.returnEmptyStringIfNull(resultSet.getString("accountCategorySec")));

            //Trade Reference details       
            List<TradeReferenceBean> tradeRefList = clientInformationBean.getTradeReferencesList();

            String companyName = resultSet.getString("companyName1");
            String contactName = resultSet.getString("companyContact1");
            TradeReferenceBean tradeReferenceBean = null;
            if (!Utilities.isNullValue(companyName) || !Utilities.isNullValue(contactName)) {
                tradeReferenceBean = new TradeReferenceBean();
                tradeReferenceBean.setCompany(Utilities.returnEmptyStringIfNull(companyName));
                ContactBean contactBean = tradeReferenceBean.getContactBean();
                contactBean.setContactName(Utilities.returnEmptyStringIfNull(contactName));
                contactBean.setPhone(Utilities.returnEmptyStringIfNull(resultSet.getString("companyPhone1")));
                contactBean.setFax(Utilities.returnEmptyStringIfNull(resultSet.getString("companyFax1")));
                tradeRefList.add(tradeReferenceBean);
            }
            companyName = resultSet.getString("companyName2");
            contactName = resultSet.getString("companyContact2");
            if (!Utilities.isNullValue(companyName) || !Utilities.isNullValue(contactName)) {
                tradeReferenceBean = new TradeReferenceBean();
                tradeReferenceBean.setCompany(Utilities.returnEmptyStringIfNull(resultSet.getString("companyName2")));
                ContactBean contactBean = tradeReferenceBean.getContactBean();
                contactBean.setContactName(Utilities.returnEmptyStringIfNull(resultSet.getString("companyContact2")));
                contactBean.setPhone(Utilities.returnEmptyStringIfNull(resultSet.getString("companyPhone2")));
                contactBean.setFax(Utilities.returnEmptyStringIfNull(resultSet.getString("companyFax2")));
                tradeRefList.add(tradeReferenceBean);
            }
            companyName = resultSet.getString("companyName3");
            contactName = resultSet.getString("companyContact3");
            if (!Utilities.isNullValue(companyName) || !Utilities.isNullValue(contactName)) {
                tradeReferenceBean.setCompany(Utilities.returnEmptyStringIfNull(resultSet.getString("companyName3")));
                ContactBean contactBean = tradeReferenceBean.getContactBean();
                contactBean.setContactName(Utilities.returnEmptyStringIfNull(resultSet.getString("companyContact3")));
                contactBean.setPhone(Utilities.returnEmptyStringIfNull(resultSet.getString("companyPhone3")));
                contactBean.setFax(Utilities.returnEmptyStringIfNull(resultSet.getString("companyFax3")));
                tradeRefList.add(tradeReferenceBean);
            }

            //.append("standardTC, standardTariff,tarrifID,docRequiredBilling,form1,form2,form3,form4,
            //statementInvoice,ediInvoice,cafPolicy,fscPolicy,ctocWarehouse,")
            RatingBillingBean ratingBillingBean = clientInformationBean.getRatingBillingBean();
            ratingBillingBean.setStandardTC(Utilities.setBillingFlag(resultSet.getString("standardTC")));
            ratingBillingBean.setStandardRatingStmt(Utilities.setBillingFlag(resultSet.getString("standardTariff")));
            ratingBillingBean.setTariffIdStmt(Utilities.setBillingFlag(resultSet.getString("tarrifID")));
            ratingBillingBean.setBillingDocs(Utilities.setBillingFlag(resultSet.getString("docRequiredBilling")));
            ratingBillingBean.setInvoicingStmt(Utilities.setBillingFlag(resultSet.getString("statementInvoice")));
            ratingBillingBean.setEdiInvoicingStmt(Utilities.setBillingFlag(resultSet.getString("ediInvoice")));
            ratingBillingBean.setCafStmt(Utilities.setBillingFlag(resultSet.getString("cafPolicy")));
            ratingBillingBean.setFscPolicyStmt(Utilities.setBillingFlag(resultSet.getString("fscPolicy")));
            ratingBillingBean.setC2cWarehouseStmt(Utilities.setBillingFlag(resultSet.getString("ctocWarehouse")));
            StringBuilder formValBuilder = new StringBuilder();
            formValBuilder.append(Utilities.returnEmptyStringIfNull(resultSet.getString("form1"))).append(" ")
                    .append(Utilities.returnEmptyStringIfNull(resultSet.getString("form2"))).append(" ")
                    .append(Utilities.returnEmptyStringIfNull(resultSet.getString("form3"))).append(" ")
                    .append(Utilities.returnEmptyStringIfNull(resultSet.getString("form4")));
            ratingBillingBean.setForm(Utilities.returnEmptyStringIfNull(formValBuilder.toString()));
            ratingBillingBean.setSpecialInstructions(Utilities.returnEmptyStringIfNull(resultSet.getString("specialInstruction")));

            Set<Map.Entry<String, String>> entrySet = Utilities.DOCUMENT_TYPES_MAP.entrySet();
            List<DocumentBean> docList = clientInformationBean.getDocumentsList();
            entrySet.forEach(entry -> {
                String key = entry.getKey();
                String docVal = null;
                try {
                    docVal = resultSet.getString(key);
                 //   System.out.println("docVal===>"+docVal);
                    if (!Utilities.isNullValue(docVal)) {
                        DocumentBean fileUploadBean = new DocumentBean();
                        fileUploadBean.setDocumentType(entry.getValue());;
                        fileUploadBean.setDocumentTypeId(key);
                        fileUploadBean.setDocumentName(docVal);
                        docList.add(fileUploadBean);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            });
            //document Information
            //.append(" ,specialInstruction,SUBSTR(specialInstruction,1,10) as subInstruction ,termsCondition,dnbiReport,salesTariif,other,powerOfAttorney,attachAddittionalSheet,credit_limit_update,unrevoke,")

           
            clientInformationBean.setCommentsList(getCommentsList(clientId));

            return clientInformationBean;

        });
        if (clientInformationBeansList != null && clientInformationBeansList.size() > 0) {
            return clientInformationBeansList.get(0);
        }

        return null;

    }
      public void deleteUploadedDocuments(Map paramMap) {
         String clientId = (String) paramMap.get("clientId");
        System.out.println("clientId===>" +clientId);
        String fileTypeId = (String) paramMap.get("fileTypeId");
        String updateQuery = "update cs_customer set " + fileTypeId + " = null where ID=?";
        System.out.println("updateQuery==>" + updateQuery);
        jdbcTemplate.update(updateQuery, new Object[]{clientId});

    }

    public List<DocumentBean> updateAndRetrieveUploadedDocuments(Map paramMap) {
        String clientId = (String) paramMap.get("clientId");
        System.out.println("clientId===>" + clientId);
        DocumentBean fileUploadBean = (DocumentBean) paramMap.get("fileUploadBean");
        String updateQuery = "update cs_customer set " + fileUploadBean.getDocumentTypeId()+ " = ? where ID=?";
        System.out.println("updateQuery==>" + updateQuery);
        jdbcTemplate.update(updateQuery, new Object[]{fileUploadBean.getActualFileName(), clientId});

        return retrieveUploadedDocuments(paramMap);

    }

    public List<DocumentBean> retrieveUploadedDocuments(Map paramMap) {
        String clientId = (String) paramMap.get("clientId");
        System.out.println("clientId===>" + clientId);

        String sqlQuery = "select ATTACHADDITTIONALSHEET,POWEROFATTORNEY,OTHER,SALESTARIIF,DNBIREPORT,TERMSCONDITION,CREDIT_LIMIT_UPDATE,UNREVOKE from cs_customer where id=?";
        Set<Map.Entry<String, String>> entrySet = Utilities.DOCUMENT_TYPES_MAP.entrySet();
        List<DocumentBean> filesList = new ArrayList<>();
        jdbcTemplate.query(sqlQuery, (ResultSet resultSet, int i) -> {

            entrySet.forEach(entry -> {
                String key = entry.getKey();
                String docVal = null;
                try {
                    docVal = resultSet.getString(key);
                    if (!Utilities.isNullValue(docVal)) {
                        DocumentBean tempBean = new DocumentBean();
                        tempBean.setDocumentType(entry.getValue());;
                        tempBean.setDocumentTypeId(key);
                        tempBean.setDocumentName(docVal);
                        filesList.add(tempBean);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            });
            return null;
        }, new Object[]{clientId});
        return filesList;
    }
    
    
     public void activateClientId(Map paramMap) {
        String customerId = (String)paramMap.get("clientId");
       
          String content= (String)paramMap.get("content");
            String emailIds= (String)paramMap.get("emailIds");
              String loggedInUserName= (String)paramMap.get("loggedInUserName");
              
        String queryUpdateAccountStatus = "UPDATE CS_CUSTOMERACCOUNTSTATUS set terms='Approved',termsCondition='Approved',standardTariff='Approved',creditAmount='Approved' where CUSTOMERID=?";
         jdbcTemplate.update(queryUpdateAccountStatus, new Object[]{customerId});
         
         
          String sqlCustomerUpdate = "UPDATE cs_customer SET status = 'Completed' "
                + "WHERE id = ? ";
        jdbcTemplate.update(sqlCustomerUpdate, new Object[]{customerId});
          int cscustomercommentId = getPKIDCSComment() + 1;
          String toIds= "";
          if(emailIds!=null){
              toIds=emailIds.replaceAll("@craneww.com", "");
          }
            String sql = "Insert into cs_customercomment(cs_customerid,customerId,cs_comment,commentDate,deniedby,REQUESTORMAILID,TONAME)"
                + "VALUES (?,?,?,current_date,?,?,?)";
        jdbcTemplate.update(sql, new Object[]{new Integer(cscustomercommentId), new Integer(customerId), content, loggedInUserName,emailIds,toIds});
     //   emailIds = emailIds+",customer.setup@craneww.com";
      
        
    }
     
     public void updateClientName(Map paramMap){
         String clientId = (String)paramMap.get("clientId");
         String clientName = (String)paramMap.get("clientName");
         String clientCode = (String)paramMap.get("clientCode");
         String query = "update cs_customer set customerName=? , CLIENTCODE=? where id=?";
         jdbcTemplate.update(query,new Object[]{clientName,clientCode,clientId});
     }
     
     

}
