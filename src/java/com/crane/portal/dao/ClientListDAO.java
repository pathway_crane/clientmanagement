/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.dao;

import com.crane.portal.bean.ClientInformationBean;
import com.crane.portal.bean.ContactBean;
import com.crane.portal.bean.DocumentBean;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apavithra
 */
public interface ClientListDAO {

    public Map getClientList(Map paramMap);

    public ClientInformationBean getClientProfileBean(Map paramMap);

    public List<ContactBean> getEscalationList(Map paramMap);

    public List<String> getApproverMailIds(Map paramMap);

    public void insertNewComment(Map paramMap);

    public void setCompletedStatus(Map paramMap);

    public void deleteUploadedDocuments(Map paramMap);

    public List<DocumentBean> updateAndRetrieveUploadedDocuments(Map paramMap);

    public void activateClientId(Map paramMap);
    
     public void updateClientName(Map paramMap);

}
