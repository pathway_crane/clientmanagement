/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.util;

import com.crane.portal.bean.DropdownBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author apavithra
 */
@Component
public  class Utilities {
    
      @Autowired
    @Qualifier("clientManagementProperties")
    private  Properties properties;

    public static final List<String> REGION_LIST = Arrays.asList("NORTAM", "LATAM", "ASPAC", "EMEA", "ALL");
    public static final String CLIENT_SETUP_EMAIL="customer.setup@craneww.com";
    public static final String SUPER_USER_ROLE = "Group[/CS_Superuser|CS_Superuser]";
    public static final String ANALYST_USER_ROLE = "Group[/CS_Analyst|CS_Analyst]";
    public static List<DropdownBean> approverTypeList = new ArrayList<>();
    public static final Map<String,String> DOCUMENT_TYPES_MAP = new HashMap();
     public static List<DropdownBean> DOCUMENT_TYPES_LIST = new ArrayList<>();
  
    public static String[] approversArr;
     public static  enum category{
        CENTRAL_APPROVER, REGIONAL_APPROVER, REGIONAL_ACCOUNTS, ANALYST, ECS_APPROVER;
    } 
    
 
     public void populateDropdowns(){
         populateApprovers();
         getAllDocTypes();
     }
          
          
   public  void populateApprovers(){
        String approvers = properties.getProperty("APPROVER_TYPES");
        String[] approverList = approvers.split(",");
        approversArr = new String[approverList.length];
        int count=0;
        for(String appr:approverList){
            String[] apprKeyVal = appr.split("~~");
            approversArr[count++]=apprKeyVal[0];
            DropdownBean dropdownBean = new DropdownBean();
            dropdownBean.setId(apprKeyVal[0]);
            dropdownBean.setDescription(apprKeyVal[1]);
            approverTypeList.add(dropdownBean);
            
        }
    }
   
   public static String setBillingFlag(String  value){
       if("Y".equalsIgnoreCase(value))
           return "YES";
       else
           return "NO";
   }

    public static String returnEmptyStringIfNull(String value) {
        String paramValue;
        if (null == value || value.equalsIgnoreCase("null")) {
            paramValue = "";
        } else {
            paramValue = value.trim();
        }
        return paramValue;
    }
    
    
    public static boolean checkNumeric(String value){
           if (value == null || value.equals("")) {
            return false;
        }
        try {
            double val = Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
      public static boolean isNullValue(String value) {
        if (value != null && !value.trim().equalsIgnoreCase("")) {
            return false;
        }
        return true;
    }
      
       public   void getAllDocTypes() {
        
        String[] strDocType = properties.getProperty("DOCUMENT_TYPE").split(",");
        for (String doc : strDocType) {
            DropdownBean dropdownBean = new DropdownBean();
            
            String[] individualDoc = doc.split("--");
            DOCUMENT_TYPES_MAP.put(individualDoc[0], individualDoc[1]);
            dropdownBean.setId(individualDoc[0]);
            dropdownBean.setDescription(individualDoc[1]);
            DOCUMENT_TYPES_LIST.add(dropdownBean);
        }
       
    }

    
}
