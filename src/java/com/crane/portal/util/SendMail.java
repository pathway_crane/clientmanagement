/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.util;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
/**
 *
 * @author apavithra
 */
public class SendMail {
     Properties props = new Properties();
    String hostName="mail.craneww.com";
    String fromAddress="Customer Setup Mgmt<info@craneww.com>";
    
    
    public void sendmailcommon(List<String> strRecipients,String strSubject,String message,String url) throws AddressException, MessagingException{
        
        
        System.out.println("Message body part is: "+message+",recipients===>"+strRecipients.toString());

        strRecipients = new ArrayList<>();
        strRecipients.add("apavithra@craneww.com");
        strRecipients.add("skumar@craneww.com");
       // StringBuilder strBuilder = new StringBuilder("List of Receipients:<br/>");
        
        InternetAddress[] recipients = new InternetAddress[strRecipients.size()]; 
        int count=0;
                for (String recipient:strRecipients){
                        recipients[count++] = new InternetAddress(recipient);
                      //System.out.println("Receipients in send mail==>"+recipient);
                }
               // recipients[strRecipients.length]=new InternetAddress("skumar@craneww.com");
    boolean debug = false;
    try {
        props.put("mail.smtp.host", hostName);
        Session session = Session.getDefaultInstance(props, null);
        session.setDebug(debug);

        MimeMessage objmsg = new MimeMessage(session);
        InternetAddress addressFrom = new InternetAddress(fromAddress);
        objmsg.setFrom(addressFrom);
        
        objmsg.setRecipients(Message.RecipientType.TO, recipients);
        //objmsg.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress("Christi.Zeno@craneww.com","skumar@craneww.com")});
        objmsg.setSubject(strSubject);
        
        objmsg.addHeader("Content-type", "text/HTML; charset=UTF-8");
        objmsg.addHeader("format", "flowed");
        objmsg.addHeader("Content-Transfer-Encoding", "8bit");
        
        message+="<a href='"+url+"'>URL</a>"+" <br/><br/>" +
                        "(Please do not reply to this notification as it is automated and is unable to receive replies) <br/><br/><br/> " +
                        "Yours sincerely, <br/> " +
                        "Customer Setup Management <br/>";
       
        objmsg.setContent(message,"text/html");
        
        

        System.out.println("Mail composed and ready to send.......");
        Transport.send(objmsg);
        System.out.println("Mail has sent to the Recipient==>"+strRecipients.toString());

    } catch (Exception e) {
        e.printStackTrace();

    }
    }
    
}
