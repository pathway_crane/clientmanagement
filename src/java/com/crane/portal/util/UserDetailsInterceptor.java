/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crane.portal.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import org.exoplatform.container.ExoContainerContext;
import org.exoplatform.services.organization.Membership;
import org.exoplatform.services.organization.OrganizationService;
import org.exoplatform.services.organization.User;
import org.springframework.web.portlet.HandlerInterceptor;
import org.springframework.web.portlet.ModelAndView;

/**
 *
 * @author apavithra
 */
public class UserDetailsInterceptor implements HandlerInterceptor {

    public boolean preHandleRender(RenderRequest request, RenderResponse response, Object handler) throws Exception {
        String userName = (String) request.getPortletSession().getAttribute("userName", PortletSession.APPLICATION_SCOPE);
        if (Utilities.isNullValue(userName)) {
            System.out.println("Getting region and role!!!");
            userName = request.getRemoteUser();
            OrganizationService orgService = (OrganizationService) ExoContainerContext.getCurrentContainer().getComponentInstanceOfType(OrganizationService.class);
            Collection<Membership> list = orgService.getMembershipHandler().findMembershipsByUser(userName);

            List<String> roleList = list.stream()
                    .distinct()
                    .map(member -> member.getMembershipType())
                    .collect(Collectors.toList());

            Optional<String> roleStr = roleList.stream()
                    .filter(member -> Utilities.REGION_LIST.contains(member))
                    .findFirst();
            String regionOfUser = roleStr.isPresent() ? roleStr.get() : "ALL";
            //has to be commented
            //  regionOfCustomer = "EMEA";
            System.out.println("regionOfUser==="+regionOfUser);
            request.getPortletSession().setAttribute("regionOfUser", regionOfUser, PortletSession.APPLICATION_SCOPE);
            Collection groupsList = orgService.getGroupHandler().findGroupsOfUser(userName);
            request.getPortletSession().setAttribute("groupsList", groupsList, PortletSession.APPLICATION_SCOPE);

            User user = orgService.getUserHandler().findUserByName(userName);

            if (user != null) {
                String loginUser = user.getFullName();
                String loginUserEmail = user.getEmail();
                request.getPortletSession().setAttribute("loginUserFullName", loginUser, PortletSession.APPLICATION_SCOPE);
                request.getPortletSession().setAttribute("loginUserEmail", loginUserEmail, PortletSession.APPLICATION_SCOPE);

            }
            String superUserFlag = "N";
           
            if ( groupsList.stream().anyMatch(str -> str.toString().trim().equals(Utilities.SUPER_USER_ROLE) || str.equals(Utilities.ANALYST_USER_ROLE))) {
              
                superUserFlag = "Y";
            }
           
            //has to be commented
            // superUserFlag = "Y";
            request.getPortletSession().setAttribute("superUserFlag", superUserFlag, PortletSession.APPLICATION_SCOPE);
            request.getPortletSession().setAttribute("userName", userName, PortletSession.APPLICATION_SCOPE);
        }
        return true;
    }

    public boolean preHandleAction(ActionRequest ar, ActionResponse ar1, Object o) throws Exception {
      
        return (ar.getPortletSession().getAttribute("userName", PortletSession.APPLICATION_SCOPE) != null);
        
    }

    public void afterActionCompletion(ActionRequest ar, ActionResponse ar1, Object o, Exception excptn) throws Exception {
        System.out.append("after action completion called!!!!!!!!!!!!!!!!!!!!!!!!");
    }

    public void postHandleRender(RenderRequest renderRequest, RenderResponse renderResponse, Object o, ModelAndView mav) throws Exception {
    }

    public void afterRenderCompletion(RenderRequest rr, RenderResponse rr1, Object o, Exception excptn) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean preHandleResource(ResourceRequest rr, ResourceResponse rr1, Object o) throws Exception {
      return (rr.getPortletSession().getAttribute("userName", PortletSession.APPLICATION_SCOPE) != null);
    }

    public void postHandleResource(ResourceRequest rr, ResourceResponse rr1, Object o, ModelAndView mav) throws Exception {
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    public void afterResourceCompletion(ResourceRequest rr, ResourceResponse rr1, Object o, Exception excptn) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean preHandleEvent(EventRequest er, EventResponse er1, Object o) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void afterEventCompletion(EventRequest er, EventResponse er1, Object o, Exception excptn) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    /*public static void main(String a[]){
        List<String> groupsList = new ArrayList<>();
        groupsList.add("Group[/CS_Superuser|CS_Superuser]");
        groupsList.add("Group[/Crane/_CWW_Employees|_CWW_Employees]");
        groupsList.add("Group[/CS_Analyst|CS_Analyst]");
        groupsList.add("Group[/platform/users|users]");
        String superUserFlag="N";
         if ( groupsList.stream().anyMatch(str -> str.equals(Utilities.SUPER_USER_ROLE) || str.equals(Utilities.ANALYST_USER_ROLE))) {
              
                superUserFlag = "Y";
            }
         System.out.println(superUserFlag);
    }*/

}
