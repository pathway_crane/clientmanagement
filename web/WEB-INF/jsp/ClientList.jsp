<%-- 
    Document   : ClientList
    Created on : 26 Mar, 2020, 4:25:16 PM
    Author     : apavithra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<portlet:defineObjects/>
<portlet:renderURL var="myRenderURL">
    <portlet:param name="action" value="viewmilestonePopup"/> 
</portlet:renderURL>
<portlet:actionURL  name="saveDefaultQuery" var="myActionURL">    
    <portlet:param name="action" value="saveDefaultQuery"/> 
</portlet:actionURL>
<html>
    <head>
        <meta charset="utf-8">
        <title> CLIENT MANAGEMENT </title>
        <meta name="description" content="">
        <meta name="author" content="">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- #CSS Links -->
        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/font-awesome.min.css">

        <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
        <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/smartadmin-production-plugins.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/smartadmin-production.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/crane_style.css">


        <link rel="stylesheet" type="text/css" media="screen"
              href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css" />



        <!-- #FAVICONS -->


        <!-- #GOOGLE FONT -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

        <script type="text/javascript">
            var __origDefine = define;
            define = null;
        </script>
    </head>
    <body >
        <form class="smart-form" style="overflow-x: hidden">
        <div class="row">

            <!-- col -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1 class="page-title txt-color-blueDark">

                    <!-- PAGE HEADER -->
                    <!--<i class="fa-fw fa fa-bar-chart-o"></i> -->
                    CLIENT LIST




                </h1>
                <!-- MAIN PANEL -->
                <div id="main" role="main">





                    <!-- MAIN CONTENT -->
                    <div id="content">
                        <div class="row">
                            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                <!-- NEW WIDGET START -->
                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget chartDiv" id="wid-id-data" data-widget-colorbutton="false" data-widget-editbutton="false"
                                     data-widget-deletebutton="false" data-widget-fullscreenbutton="false">

                                    <div>





                                        <!-- widget content -->
                                        <div class="widget-body">
                                            <ul id="myTab1" class="nav nav-tabs bordered">
                                                <li id="ALL_List">
                                                    <a  href="#s1" data-toggle="tab" onclick="loadDataTable('ALL')">ALL CLIENTS</a>
                                                </li>
                                                <li id="In_Process_List">
                                                    <a href="#s1" data-toggle="tab"  onclick="loadDataTable('In Process')">IN PROCESS CLIENTS</a>
                                                </li>
                                                <li id="Approved_List" >
                                                    <a href="#s1" data-toggle="tab" onclick="loadDataTable('Approved')">APPROVED CLIENTS</a>
                                                </li>
                                                <li id="Completed_List" >
                                                    <a href="#s1" data-toggle="tab" onclick="loadDataTable('Completed')">COMPLETED CLIENTS</a>
                                                </li>
                                                <li id="Denied_List">
                                                    <a href="#s1" data-toggle="tab" onclick="loadDataTable('Denied')">DENIED CLIENTS</a>
                                                </li>
                                                <li id="Verified_List">
                                                    <a href="#s1" data-toggle="tab" onclick="loadDataTable('Verified')">VERIFIED CLIENTS</a>
                                                </li>

                                            </ul>

                                            <div id="myTabContent1" class="tab-content padding-10">
                                                <div class="tab-pane fade in active" id="s1">

                                                    <table id="datatable_allclients" class="table table-striped table-bordered table-hover table-responsive" width="100%">
                                                        <thead>
                                                            <tr>				


                                                                <th style="width:30%" data-hide="phone">CLIENT NAME</th>
                                                                <th style="width:10%" data-hide="phone,tablet">CLIENT CODE</th>
                                                                <th data-hide="phone,tablet">REQUESTED BY</th>
                                                                <th data-hide="phone,tablet">SALES REP</th>
                                                                <th style="width:10%" data-hide="phone,tablet">STATION</th>
                                                                <th style="width:10%" data-hide="phone,tablet">STATUS</th>
                                                                <th style="width:10%" data-hide="phone,tablet"  data-field-type="dateField">SUBMIT DATE</th>

                                                                <th>ID</th>


                                                            </tr>

                                                        </thead>
                                                        <tbody>
                                                        </tbody>

                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </body>
    <form:form action="${myActionURL}"  name="clientProfileForm">

        <input type="hidden" name="submitType" value="clientProfile"/>
        <input type="hidden" name="clientId"/>
    </form:form>
    <form name="exportExcelForm">

    </form>
    <!--================================================== -->


    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
                                                        if (!window.jQuery) {
                                                            document.write('<script src="<%= request.getContextPath()%>/js/libs/jquery-3.2.1.min.js"><\/script>');
                                                        }
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
                                                        if (!window.jQuery.ui) {
                                                            document.write('<script src="<%= request.getContextPath()%>/js/libs/jquery-ui.min.js"><\/script>');
                                                        }
    </script>

    <!-- IMPORTANT: APP CONFIG -->
    <script src="<%= request.getContextPath()%>/js/app.config.js"></script>



    <!-- BOOTSTRAP JS -->
    <script src="<%= request.getContextPath()%>/js/bootstrap/bootstrap.min.js"></script>



    <!-- JARVIS WIDGETS -->
    <script src="<%= request.getContextPath()%>/js/smartwidgets/jarvis.widget.min.js"></script>



    <!-- JQUERY VALIDATE -->
    <script src="<%= request.getContextPath()%>/js/plugin/jquery-validate/jquery.validate.min.js"></script>





    <!-- FastClick: For mobile devices -->
    <script src="<%= request.getContextPath()%>/js/plugin/fastclick/fastclick.min.js"></script>

    <!--[if IE 8]>

            <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

            <![endif]-->



    <!-- MAIN APP JS FILE -->
    <script src="<%= request.getContextPath()%>/js/app.min.js"></script>





    <script src="<%= request.getContextPath()%>/js/plugin/multiselect/jquery.multi-select.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
    <script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>


    <script src="<%= request.getContextPath()%>/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= request.getContextPath()%>/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

    <script>
                                                        var otable = null;
                                                        var contextPath = '<%= request.getContextPath()%>';
                                                        let currentTab = '${currentTab}';
                                                        let clientListtype='${currentTab}';
                                                        jQuery.fn.dataTable.Api.register( 'processing()', function ( show ) {
    return this.iterator( 'table', function ( ctx ) {
        ctx.oApi._fnProcessingDisplay( ctx, show );
    } );
} );
                                                        function displayClientProfilePage(clientId) {
                                                            console.log(clientId);
                                                            document.clientProfileForm.clientId.value = clientId;
                                                            //  $("body").addClass("loading");  
                                                            document.clientProfileForm.submit();
                                                        }
                                                        function handleAjaxError(e){
                                                            alert('An error occurred on the server. Please contact administrator.');
                                                            otable.processing( false );

                                                        }
                                                        $(document).ready(function () {
                                                            pageSetUp();
                                                            $("#" + currentTab.replace(" ", "_") + "_List").addClass('active');
                                                               otable = $('#datatable_allclients').DataTable({
                                                                "processing": true,
                                                                "serverSide": true,
                                                               
                                                                "ajax": {url: "<portlet:resourceURL id="clientList"/>", "data": function (d) {
                                                                        // console.log(d);
                                                                        d.clientType = function() { return clientListtype;} ;
                                                                        d.totalColumns = d.columns.length;
                                                                        // d.custom = $('#myInput').val();
                                                                        // etc
                                                                    }, "error": handleAjaxError},
                                                                "columns": [
                                                                    {"data": "clientName",
                                                                        "render": function (data, type, row, meta) {

                                                                            data = '<a href="#" class="hyperlinkedColumn" onclick=displayClientProfilePage(' + row.clientId + ')>' + data + '</a>';
                                                                            //   data =  '<a href="#" onclick=displayClientProfilePage("'+(row.clientId+'")><u><font color="#0000FF">'+data+'</font></u></a>'


                                                                            return data;
                                                                        }},
                                                                    {"data": "clientCode"},
                                                                    {"data": "requestedBy"},
                                                                    {"data": "salesRep"},
                                                                    {"data": "station"},
                                                                    {"data": "status"},
                                                                    {"data": "submitDate"},
                                                                    {"data": "clientId"}
                                                                ],
                                                                "order": [[6, "desc"]],

                                                                // "scrollY" : "300px",
                                                                "columnDefs": [
                                                                    {
                                                                        "targets": [7],
                                                                        "visible": false,
                                                                        "searchable": false
                                                                    }],

                                                                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-12'B>>" +
                                                                        "t" + "r" +
                                                                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                                                                "language":
                                                                        {
                                                                            //"processing": "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><span class='sr-only'>Loading...</span>",
                                                                  //  processing:"<img src='<%=request.getContextPath()%>/img/ajax-loader.gif'>"
                                                                        },

                                                                "buttons": [

                                                                    {
                                                                        text: 'Export to Excel',
                                                                        key: '1',
                                                                        action: function (e, dt, node, config) {
                                                                            // console.log(dt);
                                                                            document.exportExcelForm.action = contextPath + "/exportToExcel.do";

                                                                            document.exportExcelForm.submit();

                                                                        }
                                                                    }
                                                                ],

                                                                "autoWidth": true
                                                            });
                                                            otable.column(7).visible(false);

                                                            $('#datatable_allclients thead tr').clone(true).removeClass('sorting').removeClass('sorting_asc').appendTo('#datatable_allclients thead');
                                                            $('#datatable_allclients thead tr:eq(1) th').each(function (i)
                                                            {
                                                                $(this).removeClass('sorting');
                                                                $(this).removeClass('sorting_asc');
                                                                $(this).removeClass('sorting_desc');
                                                                $(this).off('click');
                                                                var title = $(this).text();
                                                                let fieldType = $(this).attr("data-field-type");
                                                                let classNames = 'form-control form-control-sm';

                                                                if (fieldType !== undefined && fieldType === 'dateField') {
                                                                    classNames = classNames + ' datatable-date-field';
                                                                }
                                                                $(this).html('<input type="text" class="' + classNames + '" placeholder="' + title + '" />');

                                                                $('input', this).on('keyup change', function ()
                                                                {
                                                                    if (otable.column(i).search() !== this.value)
                                                                    {
                                                                        otable
                                                                                .column(i)
                                                                                .search(this.value)
                                                                                .draw();
                                                                    }
                                                                });
                                                                $(".datatable-date-field").datepicker({
                                                                    prevText: '<i class="fa fa-chevron-left"></i>',
                                                                    nextText: '<i class="fa fa-chevron-right"></i>',
                                                                    changeMonth: true, changeYear: true, dateFormat: 'mm/dd/yy',
                                                                    yearRange: '2008:' + (new Date).getFullYear()
                                                                });
                                                            });


                                                            define = __origDefine;
                                                        });
                                                        
                                                        function reloadTable(type){
                                                          
                                                        }
                                                        function loadDataTable(type) {
                                                          clientListtype=type;
                                                               $('#datatable_allclients').DataTable().ajax.reload();



                                                        

                                                        }
    </script>

</html>