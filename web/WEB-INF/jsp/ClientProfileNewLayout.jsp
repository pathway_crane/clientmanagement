<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<portlet:defineObjects/>
<portlet:renderURL var="myRenderURL">
    <portlet:param name="action" value="viewmilestonePopup"/> 
</portlet:renderURL>
<portlet:actionURL  name="saveDefaultQuery" var="myActionURL">    
    <portlet:param name="action" value="saveDefaultQuery"/> 
</portlet:actionURL>
<head>
    <meta charset="utf-8">
    <title> CLIENT PROFILE FORM </title>
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script type="text/javascript">
        var __origDefine = define;
        define = null;
    </script>

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/smartadmin-production.min.css">
    
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/crane_style.css">

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->



    <!-- #GOOGLE FONT -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
     <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/jquery.fileupload.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/jquery.fileupload-ui.css" />
     

    <script src="<%= request.getContextPath()%>/js/libs/jquery-3.2.1.min.js"></script>



    <script src="<%= request.getContextPath()%>/js/libs/jquery-ui.min.js"></script>

</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="<%= request.getContextPath()%>/js/app.config.js"></script>


<!-- BOOTSTRAP JS -->
<script src="<%= request.getContextPath()%>/js/bootstrap/bootstrap.min.js"></script>



<!-- JARVIS WIDGETS -->
<script src="<%= request.getContextPath()%>/js/smartwidgets/jarvis.widget.min.js"></script>
<script src="<%= request.getContextPath()%>/js/plugin/jquery.blockUI.js"></script>





<!--[if IE 8]>

        <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

        <![endif]-->
<c:set var="clientHeaderBean" value="${clientInformationBean.clientHeaderBean}"/>
<c:set var="creditInformationBean" value="${clientInformationBean.creditInformationBean}"/>
<c:set var="physicalAddress" value="${clientInformationBean.physicalAddress}"/>
<c:set var="billingAddress" value="${clientInformationBean.billingAddress}"/>
<c:set var="executiveContact" value="${clientInformationBean.executiveContact}"/>
<c:set var="operationsContact" value="${clientInformationBean.operationsContact}"/>
<c:set var="apContact" value="${clientInformationBean.apContact}"/>
<c:set var="ratingBillingBean" value="${clientInformationBean.ratingBillingBean}"/>
<c:set var="tradeReferencesList" value="${clientInformationBean.tradeReferencesList}"/>
<c:set var="commentsList" value="${clientInformationBean.commentsList}"/>
<c:set var="documentsList" value="${clientInformationBean.documentsList}"/>

<body >
    <div id="throbber" style="display:none;">
        <img src="<%= request.getContextPath()%>/img/ajax-loader.gif" />
    </div>
     
                  
    <form:form action="${myActionURL}" commandName="clientInformationBean" method="post" name="clientProfileForm" id="clientProfileForm" style="overflow-x: hidden" enctype="multipart/form-data">
         <div class="highslide-html-content" id="activatePartyIdDialog">
					<div class="highslide-header">
						<ul>
							<li class="highslide-move">
								<a href="#" onclick="return false">Move</a>
							</li>
							<li class="highslide-close">
								<a href="#" onclick="return hs.close(this)">Close</a>
							</li>
						</ul>
					</div>
					<div class="highslide-body" style="overflow-x: hidden;">
						<div class="container">
							<span class="fieldHeadingSmall">
								Requestor E-Mail
							</span>
							<span class="blockedSpanDark">
                                                            <input type="text" name="dlgLastReqMailId" id="dlgLastReqMailId"  style="width:170px" value="${clientHeaderBean.lastRequestorMailId}"/>

							</span>
							<span class="fieldHeadingSmall">
								Content
							</span>
							<span class="blockedSpanDark">
                                                            <textarea rows="3" cols="20" id="dlgActivateMailContent" name="dlgActivateMailContent"></textarea>

							</span>
						</div>
						<div style="float:right">
                                                    <input type="button" value="Submit" class="btn btn-primary" onclick="activateId()"/>
						</div>
					</div>
									   
					<div class="highslide-footer">
					
						<div>
							<span class="highslide-resize" title="Resize">
								<span></span>
							</span>
						</div>
					</div>
				</div>
        <input type="hidden" name="submitType" id="submitType"/>
        <input type="hidden" name="clientId" id="clientId" value="${clientHeaderBean.clientId}"/>
        <input type="hidden" name="lastReqMailId" id="lastReqMailId" />
        <input type="hidden" name="activateMailContent" id="activateMailContent" />
        <form:hidden path="clientHeaderBean.region" />
        <form:hidden path="clientHeaderBean.station" />
        <input type="hidden" name="userName" id="userName" value="${userName}" />
        <div class=" txt-color-darkgreen" style="white-space: nowrap;margin: 12px 0 12px!important;">

            <!-- PAGE HEADER -->
            <!--<i class="fa-fw fa fa-bar-chart-o"></i> -->
            <span class="page-title">
                ${clientHeaderBean.clientName}-
                ${clientHeaderBean.clientCode}
            </span>
            <div style="float:right;margin-right: 10px;">
                <span class="fieldHeadingSmall txt-color-darkgreen padding-5">
                    Create Date:${clientHeaderBean.submitDate}

                </span>

            </div>


        </div>


        <div id="main">
            <section id="widget-grid" class="">
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
                    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false"
			 data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                     
                        <header>
                            <h2><strong>General Information</strong></h2>

                        </header>
                        <div class="widget-body fuelux">

                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                <div class="col-lg-12 borderedBottom padding-bottom-10">
                                    <div class=" col-sm-6 col-md-3 col-lg-3 no-padding ">
                                        <span class="fieldHeadingSmall">
                                            Client Type

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${clientHeaderBean.clientType}

                                        </span>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-3 no-padding">
                                        <span class="fieldHeadingSmall">
                                            Company Reg. / Tax ID

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${clientHeaderBean.taxId}

                                        </span>
                                    </div>

                                    <div class=" col-sm-6 col-md-3 col-lg-3 no-padding ">
                                        <span class="fieldHeadingSmall">
                                            Selling Station

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${clientHeaderBean.station}

                                        </span>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-3 no-padding">
                                        <span class="fieldHeadingSmall">
                                            Sales Rep

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${clientHeaderBean.salesRep}

                                        </span>
                                    </div>
                                </div>

                                <div class="col-lg-12 borderedBottom padding-top-10 padding-bottom-10">
                                    <div class=" col-sm-6 col-md-3 col-lg-3 no-padding ">
                                        <span class="fieldHeadingSmall">
                                            Industry

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${clientHeaderBean.industry}

                                        </span>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-3 no-padding">
                                        <span class="fieldHeadingSmall">
                                            Sub Sector

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${clientHeaderBean.subsector}

                                        </span>
                                    </div>

                                    <div class="col-sm-6 col-md-3 col-lg-3 no-padding">
                                        <span class="fieldHeadingSmall">
                                            Requested By

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${clientHeaderBean.requestedBy}

                                        </span>
                                    </div>

                                    <div class=" col-sm-6 col-md-3 col-lg-3 no-padding ">
                                        <span class="fieldHeadingSmall">
                                            Email of Requestor

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${clientHeaderBean.initialRequestorMailId}

                                        </span>
                                    </div>
                                </div>



                            </div>





                        </div>
                    </div>


                </article>
                <article class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
                    <div class="jarviswidget" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false"
			 data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                        <header>
                            <h2><strong>Credit Information</strong> </h2>

                        </header>
                        <div class="widget-body fuelux padding-10 bordered">

                            <div class=" col-md-12 col-sm-12 col-xs-12 ">
                                <div class="col-lg-12 borderedBottom padding-bottom-10">
                                    <div class=" col-sm-6 col-md-3 col-lg-3 no-padding ">
                                        <span class="fieldHeadingSmall">
                                            Owner

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${creditInformationBean.owner}

                                        </span>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-3 no-padding">
                                        <span class="fieldHeadingSmall">
                                            Year In Business

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${creditInformationBean.yearInBusiness}

                                        </span>
                                    </div>

                                    <div class=" col-sm-6 col-md-3 col-lg-3 no-padding ">
                                        <span class="fieldHeadingSmall">
                                            Business Type

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${creditInformationBean.businessType}

                                        </span>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-3 no-padding">
                                        <span class="fieldHeadingSmall">
                                            Bank Name
                                        </span>
                                        <span class="blockedSpanDark">
                                            ${creditInformationBean.bankName}

                                        </span>
                                    </div>
                                </div>

                                <div class="col-lg-12 borderedBottom padding-top-10 padding-bottom-10">
                                    <div class=" col-sm-6 col-md-3 col-lg-3 no-padding ">
                                        <span class="fieldHeadingSmall">
                                            Bank Account

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${creditInformationBean.accountNo}

                                        </span>
                                    </div>
                                    <div class=" col-sm-6 col-md-3 col-lg-3 no-padding ">
                                        <span class="fieldHeadingSmall">
                                            Bank Phone

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${creditInformationBean.bankPhoneNo}

                                        </span>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-3 no-padding">
                                        <span class="fieldHeadingSmall">
                                            Contact

                                        </span>
                                        <span class="blockedSpanDark">
                                            ${creditInformationBean.contact}

                                        </span>
                                    </div>


                                </div>



                            </div>


                        </div>
                    </div>

                </article>
            </div>
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
                    <div class="jarviswidget" id="wid-id-paydex" data-widget-colorbutton="false" data-widget-editbutton="false"
			 data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                        <header>
                            <h2><strong>Paydex Information</strong> </h2>

                        </header>
                        <div class="widget-body fuelux padding-10 bordered">
                            <div class="col-lg-12 padding-top-10">

                                <ul id="myTab" class="nav nav-pills">
                                    <li class="active"><a href="#paydex1" data-toggle="tab"
                                                          class="no-margin">Paydex 1 </a></li>
                                    <li class=""><a href="#paydex2" data-toggle="tab">Paydex 2</a></li>

                                </ul>
                                <div id="myTabContent" class="tab-content padding-7 dark-tab">
                                    <div class="tab-pane fade active in padding-5 " id="paydex1">

                                        <div class="row borderedBottom padding-bottom-10">
                                            <div class="col-lg-12 no-padding">
                                                <div class="col-lg-4 ">
                                                    <span class="fieldHeadingSmall">Paydex Score</span>
                                                    <span class="blockedSpanDark">${creditInformationBean.dbNumber1}</span>
                                                </div>

                                                <div class="col-lg-4 ">
                                                    <span class="fieldHeadingSmall">Conservative Limit</span>
                                                    <span class="blockedSpanDark">${creditInformationBean.consLimit1}</span>
                                                </div>
                                              
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 no-padding padding-top-10">
                                                <div class="col-lg-4 ">
                                                    <span class="fieldHeadingSmall">Aggressive Limit </span>
                                                    <span class="blockedSpanDark">${creditInformationBean.aggrLimit1}</span>
                                                </div>
                                                <div class="col-lg-4">
                                                    <span class="fieldHeadingSmall">Conservative Limit Paydex Adj</span>
                                                    <span class="blockedSpanDark">${creditInformationBean.conservativeLimitPaydexAdj1}</span>
                                                </div>
                                                <div class="col-lg-4 ">
                                                    <span class="fieldHeadingSmall">Aggressive Limit Paydex Adj</span>
                                                    <span class="blockedSpanDark">${creditInformationBean.aggressLimitPaydexAdj1}</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="tab-pane fade  padding-5 " id="paydex2">

                                        <div class="row borderedBottom padding-bottom-10">
                                            <div class="col-lg-12 no-padding">
                                                <div class="col-lg-4 ">
                                                    <span class="fieldHeadingSmall">Paydex Score</span>
                                                    <span class="blockedSpanDark">${creditInformationBean.dbNumber2}</span>
                                                </div>

                                                <div class="col-lg-4 ">
                                                    <span class="fieldHeadingSmall">Conservative Limit</span>
                                                    <span class="blockedSpanDark">${creditInformationBean.consLimit2}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 no-padding">
                                                <div class="col-lg-4 ">
                                                    <span class="fieldHeadingSmall">Aggressive Limit </span>
                                                    <span class="blockedSpanDark">${creditInformationBean.aggrLimit2}</span>
                                                </div>
                                                <div class="col-lg-4">
                                                    <span class="fieldHeadingSmall">Conservative Limit Paydex Adj</span>
                                                    <span class="blockedSpanDark">${creditInformationBean.conservativeLimitPaydexAdj2}</span>
                                                </div>
                                                <div class="col-lg-4 ">
                                                    <span class="fieldHeadingSmall">Aggressive Limit Paydex Adj</span>
                                                    <span class="blockedSpanDark">${creditInformationBean.aggressLimitPaydexAdj2}</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </article>
                <article class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
                    <div class="jarviswidget" id="wid-id-creditlimit" data-widget-colorbutton="false" data-widget-editbutton="false"
			 data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                        <header>
                            <h2><strong>Credit Limit</strong> </h2>

                        </header>
                        <div class="widget-body fuelux padding-10 bordered">

                            <div class="col-lg-12  padding-top-10 padding-bottom-10 borderedBottom">
                                <div class=" col-sm-6 col-md-4 col-lg-15 no-padding ">
                                    <span class="fieldHeadingSmall">
                                        Credit Term

                                    </span>
                                    <span class="blockedSpanDark">
                                        ${creditInformationBean.creditTerm}

                                    </span>
                                </div>
                                <div class=" col-sm-6 col-md-4 col-lg-15 no-padding ">
                                    <span class="fieldHeadingSmall">
                                        Requested 

                                    </span>
                                    <span class="blockedSpanDark">
                                        ${creditInformationBean.billingCurrency1} ${creditInformationBean.creditLimit1Req}

                                    </span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-15 no-padding">
                                    <span class="fieldHeadingSmall">
                                        Approved 

                                    </span>
                                    <span class="blockedSpanDark">
                                        ${creditInformationBean.creditLimit1Approved}

                                    </span>
                                </div>




                                <div class=" col-sm-6 col-md-4 col-lg-15 no-padding flex-row ">
                                    <span class="fieldHeadingSmall">
                                        Requested Sec

                                    </span>
                                    <span class="blockedSpanDark">
                                        ${creditInformationBean.billingCurrency2} ${creditInformationBean.creditLimit2Req}

                                    </span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-15 no-padding">
                                    <span class="fieldHeadingSmall">
                                        Approved Sec

                                    </span>
                                    <span class="blockedSpanDark">
                                        ${creditInformationBean.creditLimit2Approved}

                                    </span>
                                </div>


                            </div>
                            <div class="col-lg-12  padding-top-10 padding-bottom-10 flex-row">
                                <div class=" col-sm-6 col-md-4 col-lg-15 no-padding ">
                                    <span class="fieldHeadingSmall">
                                        Acc Type

                                    </span>
                                    <span class="blockedSpanDark">
                                        ${creditInformationBean.accountType}

                                    </span>
                                </div>
                                <div class=" col-sm-6 col-md-4 col-lg-15 no-padding ">
                                    <span class="fieldHeadingSmall">
                                        Acc Category 1

                                    </span>
                                    <span class="blockedSpanDark">
                                        ${creditInformationBean.accountCategory1}

                                    </span>
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-15 no-padding">
                                    <span class="fieldHeadingSmall">
                                        Acc Category 2 

                                    </span>
                                    <span class="blockedSpanDark">
                                        ${creditInformationBean.accountCategory2}

                                    </span>
                                </div>







                            </div>

                        </div>

                    </div>
                </article>
            </div>
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-12 col-lg-3" 	>
                    <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false"
			 data-widget-deletebutton="false" data-widget-fullscreenbutton="false">

                        <header>
                            <h2><strong>Address</strong> </h2>

                        </header>
                        <div class="widget-body fuelux">
                            <div class="col-md-12 col-sm-12 col-xs-12 padding-top-10">
                                <ul id="addressTab" class="nav nav-pills">
                                    <li class="active"><a href="#physicalAddress" data-toggle="tab"
                                                          class="no-margin">Physical Address </a></li>
                                    <li class=""><a href="#billingAddress" data-toggle="tab">Billing Address</a></li>

                                </ul>
                                <div id="addressTabContent" class="tab-content padding-7 dark-tab">
                                    <div class="tab-pane fade active in padding-5 " id="physicalAddress">
                                        <div class="borderedBottom padding-bottom-10">
                                            <span class="blockedSpanDark">${physicalAddress.address1}</span>
                                            <span class="blockedSpanDark">${physicalAddress.address2} ${physicalAddress.address3}</span>
                                            <span class="blockedSpanDark">${physicalAddress.address4}</span>
                                        </div>
                                        <div class="row padding-top-10">
                                            <div class="col-lg-12 no-padding">
                                                <div class="col-lg-3 ">
                                                    <span class="fieldHeadingSmall">Country</span>
                                                    <span class="blockedSpanDark">${physicalAddress.country}</span>
                                                </div>
                                                <div class="col-lg-3">
                                                    <span class="fieldHeadingSmall">State</span>
                                                    <span class="blockedSpanDark">${physicalAddress.state}</span>
                                                </div>
                                                <div class="col-lg-3 ">
                                                    <span class="fieldHeadingSmall">Location</span>
                                                    <span class="blockedSpanDark">${physicalAddress.locationCode}</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="tab-pane fade " id="billingAddress">
                                        <div class="borderedBottom padding-bottom-10">
                                            <span class="blockedSpanDark">${billingAddress.address1}</span>
                                            <span class="blockedSpanDark">${billingAddress.address2} ${billingAddress.address3}</span>
                                            <span class="blockedSpanDark">${billingAddress.address4}</span>
                                        </div>
                                        <div class="row padding-top-10">
                                            <div class="col-lg-12 no-padding">
                                                <div class="col-lg-3 ">
                                                    <span class="fieldHeadingSmall">Country</span>
                                                    <span class="blockedSpanDark">${billingAddress.country}</span>
                                                </div>
                                                <div class="col-lg-3">
                                                    <span class="fieldHeadingSmall">State</span>
                                                    <span class="blockedSpanDark">${billingAddress.state}</span>
                                                </div>
                                                <div class="col-lg-3 ">
                                                    <span class="fieldHeadingSmall">Location</span>
                                                    <span class="blockedSpanDark">${billingAddress.locationCode}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </article>
                <article class="col-xs-12 col-sm-6 col-md-12 col-lg-3">

                    <div class="jarviswidget" id="wid-id-4" data-widget-colorbutton="false" data-widget-editbutton="false"
			 data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                        <header>
                            <h2><strong>Contact</strong> </h2>

                        </header>
                        <div class="widget-body fuelux">
                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                <ul id="contact" class="nav nav-pills">
                                    <li class="active"><a href="#executiveTab" data-toggle="tab"
                                                          class="no-margin">Executive </a></li>
                                    <li class=""><a href="#operationsTab" data-toggle="tab">Operations</a></li>
                                    <li class=""><a href="#apTab" data-toggle="tab">AP</a></li>

                                </ul>
                                <div id="contactTabContent" class="tab-content padding-7 dark-tab">
                                    <div class="tab-pane fade active in padding-5 " id="executiveTab">
                                        <div class=" padding-bottom-10">
                                            <span class="blockedSpanDark padding-bottom-5">${executiveContact.contactName}</span>
                                            <span class="blockedSpanDark padding-bottom-5"><a href="#"><u>${executiveContact.email}</u></a></span>
                                            <span class="blockedSpanDark padding-bottom-5"><i class="fa fa-phone" ></i>&nbsp;&nbsp;${executiveContact.phone}</span>
                                            <c:if test="${not empty executiveContact.mobile}">  <span class="blockedSpanDark padding-bottom-5">&nbsp;&nbsp;${executiveContact.mobile}</span></c:if>
                                            <c:if test="${not empty executiveContact.fax}"> <span class="blockedSpanDark padding-bottom-10"><i class="fa fa-fax" ></i>&nbsp;&nbsp;${executiveContact.fax}</span></c:if>
                                            </div>


                                        </div>
                                        <div class="tab-pane fade " id="operationsTab">
                                            <div class=" padding-bottom-10">
                                                <span class="blockedSpanDark padding-bottom-5">${operationsContact.contactName}</span>
                                            <span class="blockedSpanDark padding-bottom-5"><a href="#"><u>${operationsContact.email}</u></a></span>
                                            <span class="blockedSpanDark padding-bottom-5"><i class="fa fa-phone" ></i>&nbsp;&nbsp;${operationsContact.phone}</span>
                                            <c:if test="${not empty operationsContact.mobile}">       <span class="blockedSpanDark padding-bottom-5">&nbsp;&nbsp;${operationsContact.mobile}</span></c:if>
                                            <c:if test="${not empty operationsContact.fax}">  <span class="blockedSpanDark padding-bottom-10"><i class="fa fa-fax" ></i>&nbsp;&nbsp;${operationsContact.fax}</span></c:if>
                                            </div>

                                        </div>
                                        <div class="tab-pane fade " id="apTab">
                                            <div class=" padding-bottom-10">
                                                <span class="blockedSpanDark padding-bottom-5">${apContact.contactName}</span>
                                            <span class="blockedSpanDark padding-bottom-5"><a href="#"><u>${apContact.email}</u></a></span>
                                            <span class="blockedSpanDark padding-bottom-5"><i class="fa fa-phone" ></i>&nbsp;&nbsp;${apContact.phone}</span>
                                            <c:if test="${not empty apContact.mobile}">   <span class="blockedSpanDark padding-bottom-5">&nbsp;&nbsp;${apContact.mobile}</span></c:if>
                                            <c:if test="${not empty apContact.fax}"> <span class="blockedSpanDark padding-bottom-10"><i class="fa fa-fax" ></i>&nbsp;&nbsp;${apContact.fax}</span></c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </article>

                    <article class="col-xs-12 col-sm-6 col-md-12 col-lg-6">

                        <div class="jarviswidget" id="wid-id-trade-ref" data-widget-colorbutton="false" data-widget-editbutton="false"
			 data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                            <header>
                                <h2><strong>Trade Reference</strong> </h2>

                            </header>
                            <div class="widget-body fuelux">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <td>Company</td>
                                                <td>Contact Person</td>
                                                <td>Phone</td>
                                                <td>Fax</td>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        <c:choose>
                                            <c:when test="${not empty tradeReferencesList}"> 
                                                <c:forEach var="tradeReferenceBean" items="${tradeReferencesList}"> 
                                                    <tr>
                                                        <td>${tradeReferenceBean.company}</td>
                                                        <td>${tradeReferenceBean.contactBean.contactName}</td>
                                                        <td>${tradeReferenceBean.contactBean.phone}</td>
                                                        <td>${tradeReferenceBean.contactBean.fax}</td>

                                                    </tr>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise > 
                                            <td colspan="4">No Details found</td>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>

                                </table>

                             
                            </div>
                        </div>
                    </div>

                </article>



            </div>
            <div class="row"> 
                <article class="col-xs-12 col-sm-6 col-md-12 col-lg-6">

                    <div class="jarviswidget" id="wid-id-rating-billing" data-widget-colorbutton="false" data-widget-editbutton="false"
			 data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                        <header>
                            <h2><strong>Rating &amp; Billing</strong> </h2>

                        </header>
                        <div class="widget-body fuelux bor">
                            <div class="col-lg-12">
                                <div class="col-lg-3 ">
                                    <span class="fieldHeadingSmall">Standard T & C's 
                                   
                                         <span class="onoffswitch">
                                        <input type="checkbox"  class="onoffswitch-checkbox "${not empty ratingBillingBean.standardTC and ratingBillingBean.standardTC eq 'Y' ?'checked=checked':''}" id="myonoffswitch" onclick="return false;">
                                        <label class="onoffswitch-label" for="myonoffswitch"> <span class="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO" ></span> <span class="onoffswitch-switch"></span> </label> </span>


                                </div>
                                <div class="col-lg-3 ">
                                    <span class="fieldHeadingSmall">Standard Rating </span>	
                                      <span class="onoffswitch">
                                        <input type="checkbox"  class="onoffswitch-checkbox "${not empty ratingBillingBean.standardRatingStmt and ratingBillingBean.standardRatingStmt eq 'Y' ?'checked=checked':''}" id="myonoffswitch" onclick="return false;">
                                        <label class="onoffswitch-label" for="myonoffswitch"> <span class="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO" ></span> <span class="onoffswitch-switch"></span> </label> </span>

                                </div>
                                <div class="col-lg-3 ">
                                    <span class="fieldHeadingSmall">Stmt Invoicing </span>
                                      <span class="onoffswitch">
                                        <input type="checkbox"  class="onoffswitch-checkbox "${not empty ratingBillingBean.invoicingStmt and ratingBillingBean.invoicingStmt eq 'Y' ?'checked=checked':''}" id="myonoffswitch" onclick="return false;">
                                        <label class="onoffswitch-label" for="myonoffswitch"> <span class="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO" ></span> <span class="onoffswitch-switch"></span> </label> </span>


                                </div>

                                <div class="col-lg-3 ">
                                    <span class="fieldHeadingSmall" style="white-space: nowrap;">Documents for Billing</span>
                                      <span class="onoffswitch">
                                        <input type="checkbox"  class="onoffswitch-checkbox "${not empty ratingBillingBean.billingDocs and ratingBillingBean.billingDocs eq 'Y' ?'checked=checked':''}" id="myonoffswitch" onclick="return false;">
                                        <label class="onoffswitch-label" for="myonoffswitch"> <span class="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO" ></span> <span class="onoffswitch-switch"></span> </label> </span>


                                </div>

                            </div>

                            <div class="col-lg-12 borderedBottom padding-bottom-10">
                                <div class="col-lg-3 ">
                                    <span class="fieldHeadingSmall">Standard CAF </span>
                                      <span class="onoffswitch">
                                        <input type="checkbox"  class="onoffswitch-checkbox "${not empty ratingBillingBean.cafStmt and ratingBillingBean.cafStmt eq 'Y' ?'checked=checked':''}" id="myonoffswitch" onclick="return false;">
                                        <label class="onoffswitch-label" for="myonoffswitch"> <span class="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO" ></span> <span class="onoffswitch-switch"></span> </label> </span>


                                </div>
                                <div class="col-lg-3 ">
                                    <span class="fieldHeadingSmall">Standard FSC</span>
                                      <span class="onoffswitch">
                                        <input type="checkbox"  class="onoffswitch-checkbox "${not empty ratingBillingBean.fscPolicyStmt and ratingBillingBean.fscPolicyStmt eq 'Y' ?'checked=checked':''}" id="myonoffswitch" onclick="return false;">
                                        <label class="onoffswitch-label" for="myonoffswitch"> <span class="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO" ></span> <span class="onoffswitch-switch"></span> </label> </span>


                                </div>

                                <div class="col-lg-3 ">
                                    <span class="fieldHeadingSmall">EDI Invoicing </span>
                                      <span class="onoffswitch">
                                        <input type="checkbox"  class="onoffswitch-checkbox "${not empty ratingBillingBean.ediInvoicingStmt and ratingBillingBean.ediInvoicingStmt eq 'Y' ?'checked=checked':''}" id="myonoffswitch" onclick="return false;">
                                        <label class="onoffswitch-label" for="myonoffswitch"> <span class="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO" ></span> <span class="onoffswitch-switch"></span> </label> </span>


                                </div>
                                <div class="col-lg-3 ">
                                    <span class="fieldHeadingSmall">C2C Warehouse </span>
                                      <span class="onoffswitch">
                                        <input type="checkbox"  class="onoffswitch-checkbox "${not empty ratingBillingBean.c2cWarehouseStmt and ratingBillingBean.c2cWarehouseStmt eq 'Y' ?'checked=checked':''}" id="myonoffswitch" onclick="return false;">
                                        <label class="onoffswitch-label" for="myonoffswitch"> <span class="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO" ></span> <span class="onoffswitch-switch"></span> </label> </span>


                                </div>
                            </div>
                            <div class="col-lg-12 borderedBottom padding-bottom-10">
                                <div class="col-lg-3 ">
                                    <span class="fieldHeadingSmall">Form </span>
                                    <span class="blockedSpanDark">
                                        ${ratingBillingBean.form}

                                    </span>

                                </div>
                                <div class="col-lg-3 ">
                                    <span class="fieldHeadingSmall">Tariff ID# </span>
                                    <span class="blockedSpanDark">
                                        ${ratingBillingBean.tariffIdStmt}

                                    </span>

                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="col-lg-12 ">
                                    <span class="fieldHeadingSmall">
                                        Special Instructions
                                    </span>
                                    <span class="blockedSpanDark">
                                        ${ratingBillingBean.specialInstructions}

                                    </span>
                                </div>

                            </div>
                        </div>

                </article>

                <article class="col-xs-12 col-sm-6 col-md-12 col-lg-6">

                    <div class="jarviswidget" id="wid-id-fileupload" data-widget-colorbutton="false" data-widget-editbutton="false"
			 data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                        <header>
                            <h2><strong>Uploaded Documents</strong> </h2>

                        </header>
                        <div class="widget-body fuelux bor" >
                             <table id="tbFileUpload" class="table table-striped table-bordered table-hover table-condensed uploadTable">
                                                       
                                                        <tbody>
                                                            <c:choose>
                                                                <c:when  test="${not empty documentsList}">
                                                                    <c:forEach var="uploadBean" items="${documentsList}" varStatus="statusInd"> 
                                                                        <tr id="tbFileUploadrow_${statusInd.index}" class="fileUploadrow">
                                                                            <td class="text-align-left">${uploadBean.documentType}</td>
                                                                            <td class="text-align-left">
                                                                                <a href="#" onclick="displayFile(event, '${uploadBean.documentName}')">${uploadBean.documentName}</a>&nbsp;                                                                                                                               
                                                                               


                                                                            </td>
                                                                            <td> <a href="#" onclick="deleteFileUploadRow(event, '${uploadBean.documentTypeId}', '${uploadBean.documentName}', 'tbFileUploadrow_' +${statusInd.index});"><span class="glyphicon glyphicon-trash redBg"></span></a></td>
                                                                        </tr>
                                                                    </c:forEach>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <tr class="fileUploadrow">
                                                                        <td colspan="2">No files Uploaded Yet</td>
                                                                    </tr>
                                                                </c:otherwise>

                                                            </c:choose>
                                                        </tbody>
                                                    </table>

                                                    <table id="newFilesUpload" class="table table-striped table-bordered table-hover table-condensed uploadTable">

                                                        <tbody>
                                                            <tr >
                                                                <td class="text-align-left">
                                                                    <!-- <label class="labelSpecial">Type</label>-->
                                                                  
                                                                        <form:select path="documentsList[0].documentType" id="fileType0">
                                                                            <form:options items="${docTypeList}" itemValue="id" itemLabel="description"/>
                                                                        </form:select>
                                                                

                                                                </td>
                                                                <td class="text-align-left">
                                                                    <span class="btn btn-primary fileinput-button">
                                                                        <i class="glyphicon glyphicon-plus"></i>
                                                                        <span>Add files</span>

                                                                        <input type="file" name="documentsList[0].uploadedFile"  id="fileupload" multiple>
                                                                    </span>
                                                                </td>


                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <div class="alert alert-warning fade in">

                                                                        <i class="fa-fw fa fa-warning"></i>
                                                                        <b>New document uploaded would replace the existing document.</b>  
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                        </div>

                </article>
            </div>
                   
                                        <div class="row">
            <article class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                <div class="jarviswidget" id="wid-id-communication" data-widget-colorbutton="false" data-widget-editbutton="false"
			 data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <header>
                        <h2><strong>Communication</strong> </h2>

                    </header>

                    <div class="widget-body fuelux bor">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="chat-body custom-scroll" style="max-height: 599px !important;">

                                    <ul id="commentsList">

                                        <c:choose>
                                            <c:when test="${not empty commentsList}"> 
                                                <c:forEach var="commentBean" items="${commentsList}"> 
                                                    <li class="message margin-bottom-10">

                                                        <div class="message-text">
                                                            <a href="javascript:void(0);" class="username txt-color-darkRed" id="fromUserLi"><span class="communicationSpan">From:</span>&nbsp;&nbsp;${commentBean.commentFromUserId}</a> 
                                                            <a href="javascript:void(0);" class="username txt-color-blueDark" id="toUserLi"><span class="communicationSpan">To:</span>&nbsp;&nbsp;${commentBean.commentToUserId}</a> 
                                                            <span class="blockedSpanDark" id="commentLi">
                                                                ${commentBean.comment}
                                                            </span>

                                                            <time class="p-relative d-block margin-top-5" id="dateLi"> ${commentBean.commentDate}  </time> 
                                                        </div>
                                                    </li>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise > 
                                                <li class="message margin-bottom-10" style="display:none">

                                                    <div class="message-text">
                                                        <a href="javascript:void(0);" class="username txt-color-darkRed" id="fromUserLi"><span class="communicationSpan">From:</span>&nbsp;&nbsp;${commentBean.commentFromUserId}</a> 
                                                        <a href="javascript:void(0);" class="username txt-color-blueDark" id="toUserLi"><span class="communicationSpan">To:</span>&nbsp;&nbsp;${commentBean.commentToUserId}</a> 
                                                        <span class="blockedSpanDark" id="commentLi">
                                                            ${commentBean.comment}
                                                        </span>

                                                        <time class="p-relative d-block margin-top-5" id="dateLi">  </time> 
                                                    </div>
                                                </li>
                                                <li class="message margin-bottom-10" id="norecordsLi">No Comments found</li>
                                                </c:otherwise>
                                            </c:choose>
                                    </ul>
                                </div>
<span style="float:left;padding-top: 10px;"><input type="button" class="btn btn-info" value="Back to Main Page" onclick="goToPrevPage()"/>
</span>
                            </div>
                            <div class="col-lg-6" id="escalationDiv">
                                <div class="col-lg-12">
                                    <span class="fieldHeadingSmall">
                                        Comment
                                    </span>
                                    <span class="blockedSpanDark">
                                        <textarea name="comments" rows="4" cols="50" id="comments"></textarea>
                                        &nbsp;&nbsp; <input type="button" class="btn btn-primary" onclick="addComment()"
                                                            value="Add Comment" />


                                    </span>

                                </div>
                                <div class="col-lg-12">
                                    <span class="fieldHeadingSmall">
                                        Escalation
                                    </span>
                                </div>
                                <div class="col-lg-3" style="white-space: nowrap;">
                                    <span class="blockedSpanDark">
                                        By Name:



                                    </span>


                                    <span class="blockedSpanDark">
                                        <form:select  id="escalateUserName" path="clientHeaderBean.escalateUserName"  style="float:left">
                                            <option value=""></option>
                                            <form:options items="${escalationList}" itemValue="email" itemLabel="contactName"/>

                                        </form:select>

                                    </span>
                                </div>
                                <div class="col-lg-3 " style="white-space: nowrap;">
                                    <span class="blockedSpanDark">
                                        By Type:



                                    </span>


                                    <span class="blockedSpanDark">
                                        <form:select  id="approverType" path="clientHeaderBean.approverType"  style="float:left">
                                            <option value=""></option>
                                            <form:options items="${approversTypeList}" itemValue="id" itemLabel="description"/>

                                        </form:select>

                                    </span>
                                </div>

                                <div class="col-lg-3 " style="white-space: nowrap;">
                                    <span class="blockedSpanDark">
                                        Additional Mail IDs



                                    </span>


                                    <span class="blockedSpanDark">
                                        <input type="text" name="additionalEmailIds" id="additionalEmailIds"/>
                                    </span>
                                </div>
                                <div class="col-lg-12 padding-top-10">

                                    <input type="button" class="btn btn-primary" value="Escalate" onclick="escalateTo()"/>


                                </div>
                                <c:if test="${clientHeaderBean.status eq 'Approved' and  superUserFlag eq 'Y'}">
                                    <div class="col-lg-12 padding-top-10">
                                    <span class="fieldHeadingSmall">
                                        Client Code
                                    </span>
                                    <span class="blockedSpanDark">
                                        <input type="text" id="clientCode" name="clientCode" value="${clientHeaderBean.clientCode}"/>
                                        &nbsp;&nbsp;
                                        <input type="button" name="sendPartyId" value="Send to Requestor" class="btn btn-primary" onclick="return fnsendPartyId();" />&nbsp;&nbsp;
                                        <input type="button" name="activatePartyId" value="Activate Party ID" class="btn btn-primary"  onclick="return hs.htmlExpand(this, { contentId: 'activatePartyIdDialog' } )"/>
                                        
                                    </span>

                                </div>
                                </c:if>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </article>

                                        </div>
                    
                    </section>
        </div>
                                       
    </form:form>
                       <form name="downloadImageForm" method="post">

            <input type="hidden" name="fileName"/>



        </form>  
                    
</body>
<!-- Modal -->


<!-- MAIN APP JS FILE -->
<script src="<%= request.getContextPath()%>/js/app.min.js"></script>
<script src="<%= request.getContextPath()%>/js/notification/SmartNotification.min.js"></script>
 <script src="<%= request.getContextPath()%>/js/plugin/jquery.fileupload.js"></script> 
    <script src="<%= request.getContextPath()%>/js/plugin/jquery.fileupload-jquery-ui.js"></script> 
    <script src="<%= request.getContextPath()%>/js/plugin/jquery.iframe-transport.js"></script> 
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/highslide.css"/> 
    <script src="<%= request.getContextPath()%>/js/plugin/highslide-full.min.js"></script>
    <!--
    2) Optionally override the settings defined at the top
    of the highslide.js file. The parameter hs.graphicsDir is important!
-->

<script type="text/javascript">
    hs.graphicsDir = '<%= request.getContextPath()%>/css/graphics/';
    hs.outlineType = 'rounded-white';
</script>
<script lang="javascript">
      var contextPath = "<%=request.getContextPath()%>";
                                            $(document).ready(function () {

                                                pageSetUp();
                                               
                                                
                                                 $('#fileupload').fileupload({

                                                                    dataType: 'json',
                                                                    url: contextPath + '/uploadFiles.do?clientId='+$("#clientId").val(),

                                                                    done: function (e, data) {
                                                                        $("#wid-id-fileupload").unblock();
                                                                        $('.fileUploadrow').remove();

                                                                        /**    $('#progress .progress-bar').css(
                                                                         'width',
                                                                         '0%'
                                                                         );
                                                                         
                                                                         $("#progress").hide();*/


                                                                        filesUploaded = true;
                                                                        var index = $("#tbFileUpload tr").length;
                                                                        $.each(data.result, function (i, file) {

                                                                            var fileNameHtml = '<a href="#" onclick=displayFile(event,"' + file.documentName + '")>' + file.documentName + '</a>' + '&nbsp;';
                                                                            var deleteFileHtml='<a href="#" onclick="deleteFileUploadRow(event,\'' + file.documentTypeId + '\',\'' + file.documentName + '\',\'tbFileUploadrow_' + index + '\')"><span class="glyphicon glyphicon-trash redBg"></span></a>'

                                                                            $("#tbFileUpload").append(
                                                                                    $('<tr id=tbFileUploadrow_' + index + ' class="fileUploadrow"/>')
                                                                                    .append($('<td class="text-align-left"/>').text(file.documentType))

                                                                                    .append($('<td class="text-align-left"/>').html(fileNameHtml))
                                                                                     .append($('<td class="text-align-left"/>').html(deleteFileHtml))



                                                                                    );//end $("#uploaded-files").append()
                                                                            index++;

                                                                        });
                                                                        $.bigBox({

                                                                            content: "Document Uploaded Successfully",
                                                                            color: "#3276B1",
                                                                            icon : "fa fa-bell swing animated",
                                                                            timeout: 6000

                                                                        });


                                                                    },

                                                                    progressall: function (e, data) {
                                                                        /*     var progress = parseInt(data.loaded / data.total * 100, 10);
                                                                         $('#progress').show();
                                                                         $('#progress .progress-bar').css(
                                                                         'width',
                                                                         progress + '%'
                                                                         );*/
                                                                    }
                                                                }).bind('fileuploadsubmit', function (e, data) {
                                                                    // The example input, doesn't have to be part of the upload form:
                                                                    var fileUploadSubmit = 'fileUpload';
                                                                    $("#wid-id-fileupload").block({
                                                                        message: $('#throbber')

                                                                    });

                                                                    data.formData = {fileType: $('#fileType0').val()};

                                                                });

                                                define = __origDefine;
                                            });
                                            
                                            function fnsendPartyId(){
                                                   if ($("#clientCode").val() === '') {
                                                          $.bigBox({

                                                        content: "Please enter Client Code",
                                                        color: "#C46A69",
                                                        icon: "fa fa-warning shake animated",
                                                        timeout: 6000

                                                    });
                                                    return false;
                                                       
                                                   }
                                                   $("#submitType").val("sendToRequestor");
                                                     $("#clientProfileForm").block({
                                                                        message: $('#throbber')

                                                                    });
                                                                                    
                                                   document.clientProfileForm.submit();
                                            }
                                            function goToPrevPage(){
                                                  $("#submitType").val("backToClientList");
                                                    $("#clientProfileForm").block({
                                                                        message: $('#throbber')

                                                                    });
                                                                                    
                                                   document.clientProfileForm.submit();
                                            }

                                            function addComment() {
                                                
                                                if ($("#comments").val() === '') {

                                                    $.bigBox({

                                                        content: "Please enter comments",
                                                        color: "#C46A69",
                                                        icon: "fa fa-warning shake animated",
                                                        timeout: 6000

                                                    });
                                                    return false;
                                                }
                                                  $("#wid-id-communication").block({
                                                    message: $('#throbber')

                                                });
                                                
                                                 $.ajax({
                                                    type: "POST",
                                                    url: '<portlet:resourceURL id="addNewComment"/>',
                                                    data: { comments: $("#comments").val()}})
                                                        .done(function (msg) {
                                                        
                                                            addCommentRow("");
                                                             $("#wid-id-communication").unblock();
                                                             $.bigBox({

                                                                                        content: "Comment added Successfully",
                                                                                        color: "#3276B1",
                                                                                        icon : "fa fa-bell swing animated",
                                                                                        timeout: 6000

                                                                                    });

                                                        });

                                            }
                                            function activateId() {
                        //   alert("customerId==>"+$("#customerId").val()+",mailId==>"+$("#lastRequestorMailId").val());
                       let mailContent = $("#dlgActivateMailContent").val().trim();
                       let mailId =$("#dlgLastReqMailId").val().trim();
                        if (mailContent === "") {
                           
                            alert("Email Content is a required field");

                            return false;
                        }
                        if (mailId === "") {
                           

                            alert("Email Id is a required field");

                            return false;
                        }
                        var emails = mailId.replace(/\s/g, '').split(",");
                        var valid = true;
                        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                        for (var i = 0; i < emails.length; i++) {
                            if (emails[i] === "" || !regex.test(emails[i])) {
                                valid = false;
                            }
                        }
                        if (!valid) {
                            alert("Invalid format for Email Id");
                            return false;
                        }
                        $("#submitType").val("activateClientId");
                        
                        $("#lastReqMailId").val(mailId);
                        $("#activateMailContent").val(mailContent);
                       
                          $("#activatePartyIdDialog").block({
                                                                        message: $('#throbber')

                                                                    });
                                                                                    
                                                   document.clientProfileForm.submit();
                    }
                                            function escalateTo() {

                                                if ($("#approverType").val() !== '' && $("#escalateUserName").val() !== '') {
                                                    //     alert('Both Escalate By Name and Escalate By Type cannot be selected');
                                                    $.bigBox({

                                                        content: "Both Escalate By Name and Escalate By Type cannot be selected",
                                                        color: "#C46A69",
                                                        icon: "fa fa-warning shake animated",
                                                        timeout: 6000

                                                    });
                                                    return false;
                                                }
                                                if ($("#approverType").val() === '' && $("#escalateUserName").val() === '') {

                                                    $.bigBox({

                                                        content: "Either Escalate By Name and Escalate By Type should be selected",
                                                        color: "#C46A69",
                                                        icon: "fa fa-warning shake animated",
                                                           timeout: 6000

                                                    });
                                                    return false;
                                                }
                                                let escalatedName = $("#escalateUserName option:selected").text();
                                                if ($("#comments").val() === '') {

                                                    $.bigBox({

                                                        content: "Please enter comments",
                                                        color: "#C46A69",
                                                        icon: "fa fa-warning shake animated",
                                                        timeout: 6000

                                                    });
                                                    return false;
                                                }
                                                if ($("#additionalEmailIds").val() !== '') {
                                                    var emails = document.getElementById('additionalEmailIds').value.replace(/\s/g, '').split(",");
                                                    var valid = true;
                                                    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                                                    for (var i = 0; i < emails.length; i++) {
                                                        if (emails[i] === "" || !regex.test(emails[i])) {
                                                            valid = false;
                                                        }
                                                    }
                                                    if (!valid) {

                                                        $.bigBox({

                                                            content: "Invalid format for Email Id",
                                                            color: "#C46A69",
                                                            icon: "fa fa-warning shake animated",
                                                            timeout: 6000

                                                        });
                                                        return false;
                                                    }
                                                }
                                                $("#wid-id-communication").block({
                                                    message: $('#throbber')

                                                });

                                                let escalationType = 'escalateByCategory';
                                                if ($("#escalateUserName").val() !== '')
                                                    escalationType = 'escalateByName';



                                                $.ajax({
                                                    type: "POST",
                                                    url: '<portlet:resourceURL id="escalateClientId"/>',
                                                    data: {approverType: $("#approverType").val(), escalatedUserName: escalatedName, escalateUserMail: $("#escalateUserName").val(), comments: $("#comments").val(), additionalEmailIds: $("#additionalEmailIds").val(), escalationType: escalationType}})
                                                        .done(function (msg) {
                                                             let toName=$("#approverType").val().replace("_"," ")
                                                            if (escalationType === 'escalateByName') 
                                                                toName=escalatedName;                                                  

                                                          
                                                            addCommentRow(toName);
                                                             $("#wid-id-communication").unblock();
                                                             $.bigBox({

                                                                                        content: "Escalation completed",
                                                                                        color: "#3276B1",
                                                                                        icon : "fa fa-bell swing animated",
                                                                                        timeout: 6000

                                                                                    });

                                                        });


                                            }
                                            
                                            function addCommentRow(toName){
                                              var rowCount = $("#commentsList li").length + 1;
                                                            var $clone = $("#commentsList  li:first").clone();
                                                            let idVal = 'commentsList_li_' + rowCount;
                                                            $clone.attr('id', idVal);
                                                            var todayDate = new Date().toISOString().slice(0, 10);

                                                          	$clone.insertBefore('#commentsList li:first');
                                                             
                                                            $("#" + idVal).find("#commentLi").html($("#comments").val());
                                                             $("#" + idVal).find("#toUserLi").html("<span class=\"communicationSpan\">To:</span> "+toName);

                                                            $("#" + idVal).find("#fromUserLi").html("<span class=\"communicationSpan\">From:</span> "+$("#userName").val());
                                                            $("#" + idVal).find("#dateLi").html(todayDate);
                                                            $("#" + idVal).show();
                                                            $("#norecordsLi").remove();
                                                           
                                                            
                                                           
                                                            $("#escalateUserName").val("");
                                                            $("#additionalEmailIds").val("");
                                                            $("#approverType").val("");
                                                            $("#comments").val("");
                                            }
                                              function displayFile(event, fileName) {
                                                                event.preventDefault();
                                                                var url = contextPath + "/downloadFile.do";
                                                                document.downloadImageForm.action = url;
                                                                document.downloadImageForm.fileName.value = fileName;
                                                                document.downloadImageForm.submit();

                                                            }
                                                              function deleteFileUploadRow(event, fileTypeId, fileName, id) {
                                                                event.preventDefault();
                                                                $.SmartMessageBox({

                                                                    content: "Are you sure you want to delete the document?",
                                                                    buttons: '[No][Yes]'
                                                                }, function (ButtonPressed) {
                                                                    if (ButtonPressed === "Yes") {

                                                                         $("#wid-id-fileupload").block({
                                                                        message: $('#throbber')

                                                                    });


                                                                        $.ajax({
                                                                            type: "POST",
                                                                            url: contextPath + '/deleteFiles.do?clientId='+$("#clientId").val(),
                                                                            data: {fileTypeId: fileTypeId, fileName: fileName}})
                                                                                .done(function (msg) {

                                                                                    $("#" + id).remove();
                                                                                    var l = $("#tbFileUpload tr").length;
                                                                                    if (l === 0) {
                                                                                        $("#tbFileUpload").append(
                                                                                                $('<tr class="fileUploadrow"/>')
                                                                                                .append($('<td colspan="3"/>').text("No records found")));
                                                                                        filesUploaded = false;
                                                                                    }
                                                                                    
                                                                                    $("#wid-id-fileupload").unblock();
                                                                                    $.bigBox({

                                                                                        content: "Document deleted Successfully",
                                                                                        color: "#3276B1",
                                                                                        icon : "fa fa-bell swing animated",
                                                                                        timeout: 6000

                                                                                    });



                                                                                });
                                                                    }


                                                                });



                                                            }


</script>