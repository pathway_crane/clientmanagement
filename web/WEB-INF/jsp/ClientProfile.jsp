<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<portlet:defineObjects/>
<portlet:renderURL var="myRenderURL">
    <portlet:param name="action" value="viewmilestonePopup"/> 
</portlet:renderURL>
<portlet:actionURL  name="saveDefaultQuery" var="myActionURL">    
    <portlet:param name="action" value="saveDefaultQuery"/> 
</portlet:actionURL>
<head>
    <meta charset="utf-8">
    <title> CLIENT PROFILE FORM </title>
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script type="text/javascript">
        var __origDefine = define;
        define = null;
    </script>

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/smartadmin-production.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/crane_style.css">

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->



    <!-- #GOOGLE FONT -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/jquery.fileupload.css" />
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/jquery.fileupload-ui.css" />


    <script src="<%= request.getContextPath()%>/js/libs/jquery-3.2.1.min.js"></script>



    <script src="<%= request.getContextPath()%>/js/libs/jquery-ui.min.js"></script>

</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="<%= request.getContextPath()%>/js/app.config.js"></script>


<!-- BOOTSTRAP JS -->
<script src="<%= request.getContextPath()%>/js/bootstrap/bootstrap.min.js"></script>



<!-- JARVIS WIDGETS -->
<script src="<%= request.getContextPath()%>/js/smartwidgets/jarvis.widget.min.js"></script>
<script src="<%= request.getContextPath()%>/js/plugin/jquery.blockUI.js"></script>





<!--[if IE 8]>

        <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

        <![endif]-->
<c:set var="clientHeaderBean" value="${clientInformationBean.clientHeaderBean}"/>
<c:set var="creditInformationBean" value="${clientInformationBean.creditInformationBean}"/>
<c:set var="physicalAddress" value="${clientInformationBean.physicalAddress}"/>
<c:set var="billingAddress" value="${clientInformationBean.billingAddress}"/>
<c:set var="executiveContact" value="${clientInformationBean.executiveContact}"/>
<c:set var="operationsContact" value="${clientInformationBean.operationsContact}"/>
<c:set var="apContact" value="${clientInformationBean.apContact}"/>
<c:set var="ratingBillingBean" value="${clientInformationBean.ratingBillingBean}"/>
<c:set var="tradeReferencesList" value="${clientInformationBean.tradeReferencesList}"/>
<c:set var="commentsList" value="${clientInformationBean.commentsList}"/>
<c:set var="documentsList" value="${clientInformationBean.documentsList}"/>

<body >
    <div id="throbber" style="display:none;">
        <img src="<%= request.getContextPath()%>/img/ajax-loader.gif" />
    </div>


    <form:form action="${myActionURL}" commandName="clientInformationBean" method="post" name="clientProfileForm" id="clientProfileForm" style="overflow-x: hidden" enctype="multipart/form-data">
        <div class="highslide-html-content" id="activatePartyIdDialog">
            <div class="highslide-header">
                <ul>
                    <li class="highslide-move">
                        <a href="#" onclick="return false">Move</a>
                    </li>
                    <li class="highslide-close">
                        <a href="#" onclick="return hs.close(this)">Close</a>
                    </li>
                </ul>
            </div>
            <div class="highslide-body" style="overflow-x: hidden;">
                <div class="container">
                    <span class="fieldHeadingSmall">
                        Requestor E-Mail
                    </span>
                    <span class="blockedSpanDark">
                        <input type="text" name="dlgLastReqMailId" id="dlgLastReqMailId"  class="form-control modal-input" value="${clientHeaderBean.lastRequestorMailId}"/>

                    </span>
                    <span class="fieldHeadingSmall">
                        Content
                    </span>
                    <span class="blockedSpanDark">
                        <textarea rows="3" cols="30" id="dlgActivateMailContent" class="form-control modal-input" name="dlgActivateMailContent"></textarea>

                    </span>
                </div>
                <div style="float:right">
                    <input type="button" value="Submit" class="btn btn-primary" onclick="activateId()"/>
                </div>
            </div>

            <div class="highslide-footer">

                <div>
                    <span class="highslide-resize" title="Resize">
                        <span></span>
                    </span>
                </div>
            </div>
        </div>
        <input type="hidden" name="submitType" id="submitType"/>
        <input type="hidden" name="clientId" id="clientId" value="${clientHeaderBean.clientId}"/>
        <input type="hidden" name="lastReqMailId" id="lastReqMailId" />
        <input type="hidden" name="activateMailContent" id="activateMailContent" />
        <form:hidden path="clientHeaderBean.region" />
        <form:hidden path="clientHeaderBean.station" />
        <input type="hidden" name="userName" id="userName" value="${userName}" />
        <div class=" txt-color-darkgreen" style="white-space: nowrap;margin: 12px 0 12px!important;" id="clientProfileHeader">

          
            <div class="row" id="pageTitle">
                <span class="page-title" id="clientNameTxt" >
                    ${clientHeaderBean.clientName}-
                    ${clientHeaderBean.clientCode}

                </span>&nbsp;
               <c:if test="${superUserFlag eq 'Y'}">
                <a href="#" class="spanRght" id="creditLimit1Span" onclick="makeCellEditable()">
                    <span class="glyphicon glyphicon-pencil" style="font-size: 16px;"></span> 
                </a>
               </c:if>
            </div>
            <div class="row"  id="editableTitle" style="display: none;">
                <span class="page-title">
                    <input type="text" value="${clientHeaderBean.clientName}" id="clientNameUpdated"  class="text-line" style="width:35%"> - <input type="text" value="${clientHeaderBean.clientCode}" id="clientCodeUpdated"   class="text-line">&nbsp;</span><a href="#" id="saveIcon" class="spanRght"  onclick="updateClientName()">
                    <span class="glyphicon glyphicon-floppy-disk" style="font-size: 16px;"></span> 
                    
                    
                </a>	
                    <img src="<%=request.getContextPath()%>/img/loading.gif" id="loadingIcon" style="display:none"/>


            </div>


        </div>


       	<div id="main">
            <section id="widget-grid" class="">
                <div class="row">

                    <!-- NEW WIDGET START -->
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false"
                             data-widget-deletebutton="false" data-widget-fullscreenbutton="false">

                            <header>
                                <h2><strong>General Information</strong></h2>

                            </header>
                            <div class="widget-body fuelux">

                                <div class="col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="col-lg-12  padding-bottom-5">
                                        <div class=" col-sm-6 col-md-3 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Client Type

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${clientHeaderBean.clientType}

                                            </span>
                                        </div>


                                        <div class=" col-sm-6 col-md-3 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Selling Station

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${clientHeaderBean.station}

                                            </span>
                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">
                                                Sales Rep

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${clientHeaderBean.salesRep}

                                            </span>
                                        </div>
                                        <div class=" col-sm-6 col-md-3 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Industry

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${clientHeaderBean.industry}

                                            </span>
                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">
                                                Company Reg. / Tax ID

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${clientHeaderBean.taxId}

                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-lg-12  padding-bottom-5">



                                        <div class="col-sm-6 col-md-3 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">
                                                Requested By

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${clientHeaderBean.requestedBy}

                                            </span>
                                        </div>

                                        <div class=" col-sm-6 col-md-3 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Email of Requestor

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${clientHeaderBean.initialRequestorMailId}

                                            </span>
                                        </div>
                                        <div class=" col-sm-6 col-md-3 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Submit Date

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${clientHeaderBean.submitDate}

                                            </span>
                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">
                                                Sub Sector

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${clientHeaderBean.subsector}

                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 no-padding  padding-top-5 padding-bottom-5">
                                        <div class="col-sm-6 col-md-3 col-lg-6 no-padding cardLayout" >
                                            <div class="col-lg-12 padding-top-10">
                                                <span class="fieldHeadingSmall">
                                                    Physical Address

                                                </span>
                                                <span class="blockedSpanDark">
                                                    <span class="address">${physicalAddress.address1}</span>
                                                    <span class="address">${physicalAddress.address2} ${physicalAddress.address3}</span>
                                                    <span class="address">${physicalAddress.address4}</span>
                                                </span>
                                            </div>
                                            <div class="col-lg-12 padding-bottom-5 ">
                                                <div class="col-lg-4 no-padding">
                                                    <span class="fieldHeadingSmall">Country Code</span>
                                                    <span class="blockedSpanDark">${physicalAddress.country}</span>
                                                </div>
                                                <div class="col-lg-4 no-padding">
                                                    <span class="fieldHeadingSmall">State Code</span>
                                                    <span class="blockedSpanDark">${physicalAddress.state}</span>
                                                </div>
                                                <div class="col-lg-4 no-padding">
                                                    <span class="fieldHeadingSmall">Location Code</span>
                                                    <span class="blockedSpanDark">${physicalAddress.locationCode}</span>
                                                </div>



                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-md-3 col-lg-5 no-padding cardLayout">
                                            <div class="col-lg-12 padding-top-10">
                                                <span class="fieldHeadingSmall">
                                                    Billing Address

                                                </span>
                                                <span class="blockedSpanDark">
                                                    <span class="address">${billingAddress.address1}</span>
                                                    <span class="address">${billingAddress.address2} ${billingAddress.address3}</span>
                                                    <span class="address">${billingAddress.address4}</span>
                                                </span>
                                            </div>
                                            <div class="col-lg-12 padding-bottom-5 ">
                                                <div class="col-lg-4 no-padding">
                                                    <span class="fieldHeadingSmall">Country Code</span>
                                                    <span class="blockedSpanDark">${billingAddress.country}</span>
                                                </div>
                                                <div class="col-lg-4 no-padding">
                                                    <span class="fieldHeadingSmall">State Code</span>
                                                    <span class="blockedSpanDark">${billingAddress.state}</span>
                                                </div>
                                                <div class="col-lg-4 no-padding">
                                                    <span class="fieldHeadingSmall">Location Code</span>
                                                    <span class="blockedSpanDark">${billingAddress.locationCode}</span>
                                                </div>



                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-lg-12   padding-bottom-5 borderedBottom">
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Executive Contact</span>
                                            <span class="blockedSpanDark">${executiveContact.contactName}</span>
                                        </div>
                                        <div class="col-lg-3 no-padding">
                                            <span class="fieldHeadingSmall">Email</span>
                                            <span class="blockedSpanDark">${executiveContact.email}</span>
                                        </div>
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Phone</span>
                                            <span class="blockedSpanDark">${executiveContact.phone}</span>
                                        </div>
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Mobile</span>
                                            <span class="blockedSpanDark">${executiveContact.mobile}</span>
                                        </div>
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Fax</span>
                                            <span class="blockedSpanDark">${executiveContact.fax}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12   padding-bottom-5 borderedBottom">
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Operations Contact</span>
                                            <span class="blockedSpanDark">${operationsContact.contactName}</span>
                                        </div>
                                        <div class="col-lg-3 no-padding">
                                            <span class="fieldHeadingSmall">Email</span>
                                            <span class="blockedSpanDark">${operationsContact.email}</span>
                                        </div>
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Phone</span>
                                            <span class="blockedSpanDark">${operationsContact.phone}</span>
                                        </div>
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Mobile</span>
                                            <span class="blockedSpanDark">${operationsContact.mobile}</span>
                                        </div>
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Fax</span>
                                            <span class="blockedSpanDark">${operationsContact.fax}</span>
                                        </div>
                                    </div>

                                    <div class="col-lg-12   padding-bottom-5">
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">AP Contact</span>
                                            <span class="blockedSpanDark">${apContact.contactName}</span>
                                        </div>
                                        <div class="col-lg-3 no-padding">
                                            <span class="fieldHeadingSmall">Email</span>
                                            <span class="blockedSpanDark">${apContact.email}</span>
                                        </div>
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Phone</span>
                                            <span class="blockedSpanDark">${apContact.phone}</span>
                                        </div>
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Mobile</span>
                                            <span class="blockedSpanDark">${apContact.mobile}</span>
                                        </div>
                                        <div class="col-lg-2 no-padding">
                                            <span class="fieldHeadingSmall">Fax</span>
                                            <span class="blockedSpanDark">${apContact.fax}</span>
                                        </div>
                                    </div>




                                </div>





                            </div>
                        </div>


                    </article>
                    <article class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                        <div class="jarviswidget" id="wid-id-creditlimit" data-widget-colorbutton="false" data-widget-editbutton="false"
                             data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                            <header>
                                <h2><strong>Credit Information</strong> </h2>

                            </header>
                            <div class="widget-body fuelux bordered">

                                <div class=" col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="col-lg-12  padding-bottom-5">
                                        <div class=" col-sm-6 col-md-3 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Owner/Partner Name

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.owner}

                                            </span>
                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">
                                                Year In Business

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.yearInBusiness}

                                            </span>
                                        </div>

                                        <div class=" col-sm-6 col-md-3 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Type of Business

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.businessType}

                                            </span>
                                        </div>

                                    </div>

                                    <div class="col-lg-12 borderedBottom padding-bottom-5">
                                        <div class="col-sm-6 col-md-3 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">
                                                Bank Name
                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.bankName}

                                            </span>
                                        </div>
                                        <div class=" col-sm-6 col-md-3 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Bank Account

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.accountNo}

                                            </span>
                                        </div>
                                        <div class=" col-sm-6 col-md-3 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Bank Phone

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.bankPhoneNo}

                                            </span>
                                        </div>
                                        <div class="col-sm-6 col-md-3 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">
                                                Contact

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.contact}

                                            </span>
                                        </div>


                                    </div>
                                    <div class="col-lg-12 padding-top-5">
                                        <span class="fieldHeadingLarge">
                                            DB Number

                                        </span>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">1) Paydex Score</span>
                                            <span class="blockedSpanDark padding-7">${creditInformationBean.dbNumber1}</span>
                                        </div>

                                        <div class="col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">Conservative Limit</span>
                                            <span class="blockedSpanDark padding-7">${creditInformationBean.consLimit1}</span>
                                        </div>
                                        <div class="col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">Aggressive Limit </span>
                                            <span class="blockedSpanDark padding-7">${creditInformationBean.aggrLimit1}</span>
                                        </div>
                                        <div class="col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">Conservative Limit Paydex Adj</span>
                                            <span class="blockedSpanDark padding-7">${creditInformationBean.conservativeLimitPaydexAdj1}</span>
                                        </div>
                                        <div class="col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">Aggressive Limit Paydex Adj</span>
                                            <span class="blockedSpanDark padding-7">${creditInformationBean.aggressLimitPaydexAdj1}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 borderedBottom">
                                        <div class="col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">2) Paydex Score</span>
                                            <span class="blockedSpanDark padding-7">${creditInformationBean.dbNumber2}</span>
                                        </div>

                                        <div class="col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">Conservative Limit</span>
                                            <span class="blockedSpanDark padding-7">${creditInformationBean.consLimit2}</span>
                                        </div>
                                        <div class="col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">Aggressive Limit </span>
                                            <span class="blockedSpanDark padding-7">${creditInformationBean.aggrLimit2}</span>
                                        </div>
                                        <div class="col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">Conservative Limit Paydex Adj</span>
                                            <span class="blockedSpanDark padding-7">${creditInformationBean.conservativeLimitPaydexAdj2}</span>
                                        </div>
                                        <div class="col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">Aggressive Limit Paydex Adj</span>
                                            <span class="blockedSpanDark padding-7">${creditInformationBean.aggressLimitPaydexAdj2}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 padding-top-5 ">
                                        <div class=" col-sm-6 col-md-4 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Credit Term

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.creditTerm}

                                            </span>
                                        </div>
                                        <div class=" col-sm-6 col-md-4 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                1) Credit Limit Requested

                                            </span>
                                            <span class="blockedSpanDark padding-7">
                                                ${creditInformationBean.creditLimit1Req}

                                            </span>
                                        </div>
                                        <div class="col-sm-6 col-md-4 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">
                                                1) Credit Limit Approved 

                                            </span>
                                            <span class="blockedSpanDark padding-7">
                                                ${creditInformationBean.creditLimit1Approved}

                                            </span>
                                        </div>
                                        <div class="col-sm-6 col-md-4 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">
                                                1) Billing Currency 

                                            </span>
                                            <span class="blockedSpanDark ">
                                                ${creditInformationBean.billingCurrency1}

                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12   borderedBottom">
                                        <div class=" col-sm-6 col-md-4 col-lg-15 no-padding  ">
                                            <span class="fieldHeadingSmall">
                                                &nbsp;

                                            </span>
                                            <span class="blockedSpanDark">
                                                &nbsp;

                                            </span>
                                        </div>
                                        <div class=" col-sm-6 col-md-4 col-lg-15 no-padding  ">
                                            <span class="fieldHeadingSmall ">
                                                2) Credit Limit Requested

                                            </span>
                                            <span class="blockedSpanDark padding-7">
                                                ${creditInformationBean.creditLimit2Req}

                                            </span>
                                        </div>
                                        <div class="col-sm-6 col-md-4 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall ">
                                                2) Credit Limit Approved

                                            </span>
                                            <span class="blockedSpanDark padding-7">
                                                ${creditInformationBean.creditLimit2Approved}

                                            </span>
                                        </div>
                                        <div class="col-sm-6 col-md-4 col-lg-15 no-padding">
                                            <span class="fieldHeadingSmall">
                                                2) Billing Currency 

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.billingCurrency2} 

                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 padding-top-5 ">
                                        <div class=" col-sm-6 col-md-4 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                Account Type

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.accountType}

                                            </span>
                                        </div>
                                        <div class=" col-sm-6 col-md-4 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                1) Account Category

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.accountCategory1}

                                            </span>
                                        </div>
                                        <div class=" col-sm-6 col-md-4 col-lg-15 no-padding ">
                                            <span class="fieldHeadingSmall">
                                                2) Account Category 

                                            </span>
                                            <span class="blockedSpanDark">
                                                ${creditInformationBean.accountCategory2}

                                            </span>
                                        </div>
                                    </div>



                                </div>

                            </div>

                        </div>
                    </article>

                </div>

                <div class="row">


                    <article class="col-xs-12 col-sm-6 col-md-12 col-lg-12">

                        <div class="jarviswidget" id="wid-id-trade-ref" data-widget-colorbutton="false" data-widget-editbutton="false"
                             data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                            <header>
                                <h2><strong>Trade Reference</strong> </h2>

                            </header>
                            <div class="widget-body fuelux">
                                <div class="table-responsive">
                                    <table class="table  table-bordered">
                                        <thead>
                                            <tr>
                                                <td>Company</td>
                                                <td>Contact Person</td>
                                                <td>Phone</td>
                                                <td>Fax</td>
                                            </tr>
                                        </thead>

                                        <tbody>									


                                            <c:choose>
                                                <c:when test="${not empty tradeReferencesList}"> 
                                                    <c:forEach var="tradeReferenceBean" items="${tradeReferencesList}"> 
                                                        <tr>
                                                            <td>${tradeReferenceBean.company}</td>
                                                            <td>${tradeReferenceBean.contactBean.contactName}</td>
                                                            <td>${tradeReferenceBean.contactBean.phone}</td>
                                                            <td>${tradeReferenceBean.contactBean.fax}</td>

                                                        </tr>
                                                    </c:forEach>
                                                </c:when>
                                                <c:otherwise > 
                                                <td colspan="4">No Details found</td>
                                            </c:otherwise>
                                        </c:choose>


                                        </tbody>

                                    </table>


                                </div>
                            </div>
                        </div>

                    </article>



                </div>
                <div class="row"> 
                    <article class="col-xs-12 col-sm-6 col-md-12 col-lg-12">

                        <div class="jarviswidget" id="wid-id-rating-billing" data-widget-colorbutton="false" data-widget-editbutton="false"
                             data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                            <header>
                                <h2><strong>Rating &amp; Billing</strong> </h2>

                            </header>
                            <div class="widget-body fuelux bor">
                                <div class="col-lg-12">
                                    <div class="col-lg-15 ">
                                        <span class="fieldHeadingSmall">Standard T & C's </span>

                                        <span class="onoffswitch">
                                            ${ratingBillingBean.standardTC}
                                        </span>


                                    </div>
                                    <div class="col-lg-15 ">
                                        <span class="fieldHeadingSmall">Standard Rating </span>	
                                        <span class="onoffswitch">
                                            ${ratingBillingBean.standardRatingStmt}
                                        </span>

                                    </div>
                                    <div class="col-lg-15 ">
                                        <span class="fieldHeadingSmall">Stmt Invoicing </span>
                                        <span class="onoffswitch">
                                            ${ratingBillingBean.invoicingStmt}
                                        </span>


                                    </div>

                                    <div class="col-lg-15 ">
                                        <span class="fieldHeadingSmall" style="white-space: nowrap;">Documents for Billing</span>
                                        <span class="onoffswitch">
                                            ${ratingBillingBean.billingDocs}
                                        </span>


                                    </div>
                                    <div class="col-lg-15 ">
                                        <span class="fieldHeadingSmall">Standard CAF </span>
                                        <span class="onoffswitch">
                                            ${ratingBillingBean.cafStmt}
                                        </span>


                                    </div>

                                </div>

                                <div class="col-lg-12 borderedBottom padding-bottom-5">

                                    <div class="col-lg-15 ">
                                        <span class="fieldHeadingSmall">Standard FSC</span>
                                        <span class="onoffswitch">
                                            ${ratingBillingBean.fscPolicyStmt}
                                        </span>


                                    </div>

                                    <div class="col-lg-15 ">
                                        <span class="fieldHeadingSmall">EDI Invoicing </span>
                                        <span class="onoffswitch">
                                            ${ratingBillingBean.ediInvoicingStmt}
                                        </span>


                                    </div>
                                    <div class="col-lg-15 ">
                                        <span class="fieldHeadingSmall">C2C Warehouse </span>
                                        <span class="onoffswitch">
                                            ${ratingBillingBean.c2cWarehouseStmt}
                                        </span>


                                    </div>
                                    <div class="col-lg-15 ">
                                        <span class="fieldHeadingSmall">Form </span>
                                        <span class="blockedSpanDark">
                                            ${ratingBillingBean.form}

                                        </span>

                                    </div>
                                    <div class="col-lg-15 ">
                                        <span class="fieldHeadingSmall">Tariff ID# </span>
                                        <span class="blockedSpanDark">
                                            ${ratingBillingBean.tariffIdStmt}

                                        </span>

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="col-lg-12 no-padding">
                                        <span class="fieldHeadingSmall">
                                            Special Instructions
                                        </span>
                                        <span class="blockedSpanDark">
                                            ${ratingBillingBean.specialInstructions}

                                        </span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="row"> 
                    <article class="col-xs-12 col-sm-6 col-md-12 col-lg-12">

                        <div class="jarviswidget" id="wid-id-fileupload" data-widget-colorbutton="false" data-widget-editbutton="false"
                             data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                            <header>
                                <h2><strong>Documents Attached</strong> </h2>

                            </header>
                            <div class="widget-body fuelux bor" >
                                <div class="row">
                                    <div class=" col-lg-6 padding-top-5">
                                        <table id="tbFileUpload" class="table  table-bordered table-hover table-condensed uploadTable">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        Document Type
                                                    </td>
                                                    <td>
                                                        Document Name
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </thead>		   
                                            <tbody>
                                                <c:choose>
                                                    <c:when  test="${not empty documentsList}">
                                                        <c:forEach var="uploadBean" items="${documentsList}" varStatus="statusInd"> 
                                                            <tr id="tbFileUploadrow_${statusInd.index}" class="fileUploadrow">
                                                                <td class="text-align-left">${uploadBean.documentType}</td>
                                                                <td class="text-align-left">
                                                                    <a href="#" onclick="displayFile(event, '${uploadBean.documentName}')">${uploadBean.documentName}</a>&nbsp;                                                                                                                               



                                                                </td>
                                                                <td> <a href="#" onclick="deleteFileUploadRow(event, '${uploadBean.documentTypeId}', '${uploadBean.documentName}', 'tbFileUploadrow_' +${statusInd.index});"><span class="glyphicon glyphicon-trash redBg"></span></a></td>
                                                            </tr>
                                                        </c:forEach>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <tr class="fileUploadrow">
                                                            <td colspan="2">No files Uploaded Yet</td>
                                                        </tr>
                                                    </c:otherwise>

                                                </c:choose>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class=" col-lg-6">
                                        <table id="newFilesUpload" class="table  table-bordered table-hover table-condensed uploadTable">
                                            <thead>
                                                <tr>
                                                    <td colspan="2">
                                                        File Upload
                                                    </td>


                                                </tr>
                                            </thead>		   
                                            <tbody>
                                                <tr >
                                                    <td class="text-align-left">
                                                        <!-- <label class="labelSpecial">Type</label>-->

                                                        <form:select path="documentsList[0].documentType" id="fileType0">
                                                            <form:options items="${docTypeList}" itemValue="id" itemLabel="description"/>
                                                        </form:select>


                                                    </td>
                                                    <td class="text-align-left">
                                                        <span class="btn btn-primary fileinput-button">
                                                            <i class="glyphicon glyphicon-plus"></i>
                                                            <span>Add files</span>

                                                            <input type="file" name="documentsList[0].uploadedFile"  id="fileupload" multiple>
                                                        </span>
                                                    </td>


                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div class="alert alert-warning fade in">

                                                            <i class="fa-fw fa fa-warning"></i>
                                                            <b>New document uploaded would replace the existing document.</b>  
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </article>
                </div>

                <div class="row">
                    <article class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                        <div class="jarviswidget" id="wid-id-communication" data-widget-colorbutton="false" data-widget-editbutton="false"
                             data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                            <header>
                                <h2><strong>Communication</strong> </h2>

                            </header>

                            <div class="widget-body fuelux bor">
                                <div class="row">

                                    <div class="col-lg-11  padding-bottom-5">
                                        <div class="chat-body custom-scroll no-padding" style="max-height: 499px !important;">

                                            <ul id="commentsList">

                                                <c:choose>
                                                    <c:when test="${not empty commentsList}"> 
                                                        <c:forEach var="commentBean" items="${commentsList}"> 
                                                            <li class="message margin-bottom-10">

                                                                <div class="message-text">
                                                                    <a href="javascript:void(0);" class="username txt-color-darkRed" id="fromUserLi"><span class="communicationSpan">From:</span>&nbsp;&nbsp;${commentBean.commentFromUserId}</a> 
                                                                    <a href="javascript:void(0);" class="username txt-color-blueDark" id="toUserLi"><span class="communicationSpan">To:</span>&nbsp;&nbsp;${commentBean.commentToUserId}</a> 
                                                                    <span class="blockedSpanDark" id="commentLi">
                                                                        ${commentBean.comment}
                                                                    </span>

                                                                    <time class="p-relative d-block margin-top-5" id="dateLi"> ${commentBean.commentDate}  </time> 
                                                                </div>
                                                            </li>
                                                        </c:forEach>
                                                    </c:when>
                                                    <c:otherwise > 
                                                        <li class="message margin-bottom-10" style="display:none">

                                                            <div class="message-text">
                                                                <a href="javascript:void(0);" class="username txt-color-darkRed" id="fromUserLi"><span class="communicationSpan">From:</span>&nbsp;&nbsp;${commentBean.commentFromUserId}</a> 
                                                                <a href="javascript:void(0);" class="username txt-color-blueDark" id="toUserLi"><span class="communicationSpan">To:</span>&nbsp;&nbsp;${commentBean.commentToUserId}</a> 
                                                                <span class="blockedSpanDark" id="commentLi">
                                                                    ${commentBean.comment}
                                                                </span>

                                                                <time class="p-relative d-block margin-top-5" id="dateLi">  </time> 
                                                            </div>
                                                        </li>
                                                        <li class="message margin-bottom-10" id="norecordsLi">No Comments found</li>
                                                        </c:otherwise>
                                                    </c:choose>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                        <div class="jarviswidget" data-widget-colorbutton="false" data-widget-editbutton="false"
                             data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                            <div></div>

                            <div class="widget-body fuelux bor">
                                <div class="row">
                                    <div class="col-lg-12" id="escalationDiv">

                                        <div class="col-lg-4">
                                            <span class="fieldHeadingSmall">
                                                Comment
                                            </span>
                                            <span class="blockedSpanDark">
                                                <textarea name="comments" rows="4" cols="50" id="comments"></textarea>
                                                &nbsp;&nbsp; 


                                            </span>


                                        </div>
                                        <div class="col-lg-8">
                                            <div class="col-lg-12">
                                                <span class="fieldHeadingLarge">
                                                    Escalation
                                                </span>
                                            </div>
                                            <div class="col-lg-3" style="white-space: nowrap;">
                                                <span class="fieldHeadingSmall">
                                                    By Name:



                                                </span>


                                                <span class="blockedSpanDark">
                                                    <form:select  id="escalateUserName" path="clientHeaderBean.escalateUserName"  style="float:left">
                                                        <option value=""></option>
                                                        <form:options items="${escalationList}" itemValue="email" itemLabel="contactName"/>

                                                    </form:select>

                                                </span>
                                            </div>
                                            <div class="col-lg-3 " style="white-space: nowrap;">
                                                <span class="fieldHeadingSmall">
                                                    By Type:



                                                </span>


                                                <span class="blockedSpanDark">
                                                    <form:select  id="approverType" path="clientHeaderBean.approverType"  style="float:left">
                                                        <option value=""></option>
                                                        <form:options items="${approversTypeList}" itemValue="id" itemLabel="description"/>

                                                    </form:select>


                                                </span>
                                            </div>

                                            <div class="col-lg-3 " style="white-space: nowrap;">
                                                <span class="fieldHeadingSmall">
                                                    Additional Mail IDs



                                                </span>


                                                <span class="blockedSpanDark">
                                                    <input type="text" name="additionalEmailIds" id="additionalEmailIds"/>
                                                </span>
                                            </div>
                                            <div class="col-lg-12" style="white-space: nowrap;">

                                                <span class="blockedSpanDark">
                                                    <input type="button" class="btn btn-primary" value="Escalate" onclick="escalateTo()"/>
                                                </span>
                                            </div>

                                        </div>


                                        <div class="col-lg-12 padding-top-5">


                                            <input type="button" class="btn btn-primary" onclick="addComment()"
                                                   value="Post Comment" />

                                        </div>
                                        <c:if test="${clientHeaderBean.status eq 'Approved' and  superUserFlag eq 'Y'}">
                                            <div class="col-lg-12 padding-top-5">
                                                <span class="fieldHeadingSmall">
                                                    Client Code
                                                </span>
                                                <span class="blockedSpanDark">
                                                    <input type="text" id="clientCode" name="clientCode" value="${clientHeaderBean.clientCode}"/>
                                                    &nbsp;&nbsp;
                                                    <input type="button" name="sendPartyId" value="Send to Requestor" class="btn btn-primary" onclick="return fnsendPartyId();" />&nbsp;&nbsp;
                                                    <input type="button" name="activatePartyId" value="Activate Party ID" class="btn btn-primary"  onclick="return hs.htmlExpand(this, {contentId: 'activatePartyIdDialog'})"/>

                                                </span>

                                            </div>
                                        </c:if>



                                    </div>
                                </div>





                            </div>
                        </div>
                    </article>
                    <article class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                        <span class="" style="float:left;padding-top: 10px;"><input type="button" class="btn btn-info" value="Back to Main Page" onclick="goToPrevPage()"/>
                        </span>
                    </article>

                </div>

            </section>
        </div>

    </form:form>
    <form name="downloadImageForm" method="post">

        <input type="hidden" name="fileName"/>



    </form>  

</body>
<!-- Modal -->


<!-- MAIN APP JS FILE -->
<script src="<%= request.getContextPath()%>/js/app.min.js"></script>
<script src="<%= request.getContextPath()%>/js/notification/SmartNotification.min.js"></script>
<script src="<%= request.getContextPath()%>/js/plugin/jquery.fileupload.js"></script> 
<script src="<%= request.getContextPath()%>/js/plugin/jquery.fileupload-jquery-ui.js"></script> 
<script src="<%= request.getContextPath()%>/js/plugin/jquery.iframe-transport.js"></script> 
<link rel="stylesheet" type="text/css" media="screen" href="<%= request.getContextPath()%>/css/highslide.css"/> 
<script src="<%= request.getContextPath()%>/js/plugin/highslide-full.min.js"></script>
<!--
2) Optionally override the settings defined at the top
of the highslide.js file. The parameter hs.graphicsDir is important!
-->

<script type="text/javascript">
                                            hs.graphicsDir = '<%= request.getContextPath()%>/css/graphics/';
                                            hs.outlineType = 'rounded-white';
</script>
<script lang="javascript">
    var contextPath = "<%=request.getContextPath()%>";
    $(document).ready(function () {

        pageSetUp();


        $('#fileupload').fileupload({

            dataType: 'json',
            url: contextPath + '/uploadFiles.do?clientId=' + $("#clientId").val(),

            done: function (e, data) {
                $("#wid-id-fileupload").unblock();
                $('.fileUploadrow').remove();

                /**    $('#progress .progress-bar').css(
                 'width',
                 '0%'
                 );
                 
                 $("#progress").hide();*/


                filesUploaded = true;
                var index = $("#tbFileUpload tr").length;
                $.each(data.result, function (i, file) {

                    var fileNameHtml = '<a href="#" onclick=displayFile(event,"' + file.documentName + '")>' + file.documentName + '</a>' + '&nbsp;';
                    var deleteFileHtml = '<a href="#" onclick="deleteFileUploadRow(event,\'' + file.documentTypeId + '\',\'' + file.documentName + '\',\'tbFileUploadrow_' + index + '\')"><span class="glyphicon glyphicon-trash redBg"></span></a>'

                    $("#tbFileUpload tbody").append(
                            $('<tr id=tbFileUploadrow_' + index + ' class="fileUploadrow"/>')
                            .append($('<td class="text-align-left"/>').text(file.documentType))

                            .append($('<td class="text-align-left"/>').html(fileNameHtml))
                            .append($('<td class="text-align-left"/>').html(deleteFileHtml))



                            );//end $("#uploaded-files").append()
                    index++;

                });
                $.bigBox({

                    content: "Document Uploaded Successfully",
                    color: "#3276B1",
                    icon: "fa fa-bell swing animated",
                    timeout: 6000

                });


            },

            progressall: function (e, data) {
                /*     var progress = parseInt(data.loaded / data.total * 100, 10);
                 $('#progress').show();
                 $('#progress .progress-bar').css(
                 'width',
                 progress + '%'
                 );*/
            }
        }).bind('fileuploadsubmit', function (e, data) {
            // The example input, doesn't have to be part of the upload form:
            var fileUploadSubmit = 'fileUpload';
            $("#wid-id-fileupload").block({
                message: $('#throbber')

            });

            data.formData = {fileType: $('#fileType0').val()};

        });

        define = __origDefine;
    });

    function fnsendPartyId() {
        if ($("#clientCode").val() === '') {
            $.bigBox({

                content: "Please enter Client Code",
                color: "#C46A69",
                icon: "fa fa-warning shake animated",
                timeout: 6000

            });
            return false;

        }
        $("#submitType").val("sendToRequestor");
        $("#clientProfileForm").block({
            message: $('#throbber')

        });

        document.clientProfileForm.submit();
    }
    function goToPrevPage() {
        $("#submitType").val("backToClientList");
        $("#clientProfileForm").block({
            message: $('#throbber')

        });

        document.clientProfileForm.submit();
    }
    function makeCellEditable() {
        $("#pageTitle").hide();
        $("#editableTitle").show();
        $("#clientNameUpdated").focus();
    }
    function updateClientName() {
        if ($("#clientNameUpdated").val().trim() === '') {

            $.bigBox({

                content: "Please enter Client Name",
                color: "#C46A69",
                icon: "fa fa-warning shake animated",
                timeout: 6000

            });
            return false;
        }
      $("#loadingIcon").show();
      $("#saveIcon").hide();

        $.ajax({
            type: "POST",
            url: '<portlet:resourceURL id="updateClientName"/>',
            data: {clientName: $("#clientNameUpdated").val(), clientCode: $("#clientCodeUpdated").val()}})
                .done(function (msg) {
                    $("#clientNameTxt").html($("#clientNameUpdated").val() + " - " + $("#clientCodeUpdated").val());
               $("#loadingIcon").hide();
      $("#saveIcon").show();
                    $("#pageTitle").show();
                    $("#editableTitle").hide();
                   
                });

    }

    function addComment() {

        if ($("#comments").val() === '') {

            $.bigBox({

                content: "Please enter comments",
                color: "#C46A69",
                icon: "fa fa-warning shake animated",
                timeout: 6000

            });
            return false;
        }
        $("#wid-id-communication").block({
            message: $('#throbber')

        });

        $.ajax({
            type: "POST",
            url: '<portlet:resourceURL id="addNewComment"/>',
            data: {comments: $("#comments").val()}})
                .done(function (msg) {

                    addCommentRow("");
                    $("#wid-id-communication").unblock();
                    $.bigBox({

                        content: "Comment added Successfully",
                        color: "#3276B1",
                        icon: "fa fa-bell swing animated",
                        timeout: 6000

                    });

                });

    }
    function activateId() {
        //   alert("customerId==>"+$("#customerId").val()+",mailId==>"+$("#lastRequestorMailId").val());
        let mailContent = $("#dlgActivateMailContent").val().trim();
        let mailId = $("#dlgLastReqMailId").val().trim();
        if (mailContent === "") {

            alert("Email Content is a required field");

            return false;
        }
        if (mailId === "") {


            alert("Email Id is a required field");

            return false;
        }
        var emails = mailId.replace(/\s/g, '').split(",");
        var valid = true;
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        for (var i = 0; i < emails.length; i++) {
            if (emails[i] === "" || !regex.test(emails[i])) {
                valid = false;
            }
        }
        if (!valid) {
            alert("Invalid format for Email Id");
            return false;
        }
        $("#submitType").val("activateClientId");

        $("#lastReqMailId").val(mailId);
        $("#activateMailContent").val(mailContent);

        $("#activatePartyIdDialog").block({
            message: $('#throbber')

        });

        document.clientProfileForm.submit();
    }
    function escalateTo() {

        if ($("#approverType").val() !== '' && $("#escalateUserName").val() !== '') {
            //     alert('Both Escalate By Name and Escalate By Type cannot be selected');
            $.bigBox({

                content: "Both Escalate By Name and Escalate By Type cannot be selected",
                color: "#C46A69",
                icon: "fa fa-warning shake animated",
                timeout: 6000

            });
            return false;
        }
        if ($("#approverType").val() === '' && $("#escalateUserName").val() === '') {

            $.bigBox({

                content: "Either Escalate By Name and Escalate By Type should be selected",
                color: "#C46A69",
                icon: "fa fa-warning shake animated",
                timeout: 6000

            });
            return false;
        }
        let escalatedName = $("#escalateUserName option:selected").text();
        if ($("#comments").val() === '') {

            $.bigBox({

                content: "Please enter comments",
                color: "#C46A69",
                icon: "fa fa-warning shake animated",
                timeout: 6000

            });
            return false;
        }
        if ($("#additionalEmailIds").val() !== '') {
            var emails = document.getElementById('additionalEmailIds').value.replace(/\s/g, '').split(",");
            var valid = true;
            var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            for (var i = 0; i < emails.length; i++) {
                if (emails[i] === "" || !regex.test(emails[i])) {
                    valid = false;
                }
            }
            if (!valid) {

                $.bigBox({

                    content: "Invalid format for Email Id",
                    color: "#C46A69",
                    icon: "fa fa-warning shake animated",
                    timeout: 6000

                });
                return false;
            }
        }
        $("#wid-id-communication").block({
            message: $('#throbber')

        });

        let escalationType = 'escalateByCategory';
        if ($("#escalateUserName").val() !== '')
            escalationType = 'escalateByName';



        $.ajax({
            type: "POST",
            url: '<portlet:resourceURL id="escalateClientId"/>',
            data: {approverType: $("#approverType").val(), escalatedUserName: escalatedName, escalateUserMail: $("#escalateUserName").val(), comments: $("#comments").val(), additionalEmailIds: $("#additionalEmailIds").val(), escalationType: escalationType}})
                .done(function (msg) {
                    let toName = $("#approverType").val().replace("_", " ")
                    if (escalationType === 'escalateByName')
                        toName = escalatedName;


                    addCommentRow(toName);
                    $("#wid-id-communication").unblock();
                    $.bigBox({

                        content: "Escalation completed",
                        color: "#3276B1",
                        icon: "fa fa-bell swing animated",
                        timeout: 6000

                    });

                });


    }

    function addCommentRow(toName) {
        var rowCount = $("#commentsList li").length + 1;
        var $clone = $("#commentsList  li:first").clone();
        let idVal = 'commentsList_li_' + rowCount;
        $clone.attr('id', idVal);
        var todayDate = new Date().toISOString().slice(0, 10);

        $clone.insertBefore('#commentsList li:first');

        $("#" + idVal).find("#commentLi").html($("#comments").val());
        $("#" + idVal).find("#toUserLi").html("<span class=\"communicationSpan\">To:</span> " + toName);

        $("#" + idVal).find("#fromUserLi").html("<span class=\"communicationSpan\">From:</span> " + $("#userName").val());
        $("#" + idVal).find("#dateLi").html(todayDate);
        $("#" + idVal).show();
        $("#norecordsLi").remove();



        $("#escalateUserName").val("");
        $("#additionalEmailIds").val("");
        $("#approverType").val("");
        $("#comments").val("");
    }
    function displayFile(event, fileName) {
        event.preventDefault();
        var url = contextPath + "/downloadFile.do";
        document.downloadImageForm.action = url;
        document.downloadImageForm.fileName.value = fileName;
        document.downloadImageForm.submit();

    }
    function deleteFileUploadRow(event, fileTypeId, fileName, id) {
        event.preventDefault();
        $.SmartMessageBox({

            content: "Are you sure you want to delete the document?",
            buttons: '[No][Yes]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Yes") {

                $("#wid-id-fileupload").block({
                    message: $('#throbber')

                });


                $.ajax({
                    type: "POST",
                    url: contextPath + '/deleteFiles.do?clientId=' + $("#clientId").val(),
                    data: {fileTypeId: fileTypeId, fileName: fileName}})
                        .done(function (msg) {

                            $("#" + id).remove();
                            var l = $("#tbFileUpload tbody tr").length;
                            if (l === 0) {
                                $("#tbFileUpload").append(
                                        $('<tr class="fileUploadrow"/>')
                                        .append($('<td colspan="3"/>').text("No records found")));
                                filesUploaded = false;
                            }

                            $("#wid-id-fileupload").unblock();
                            $.bigBox({

                                content: "Document deleted Successfully",
                                color: "#3276B1",
                                icon: "fa fa-bell swing animated",
                                timeout: 6000

                            });



                        });
            }


        });



    }


</script>